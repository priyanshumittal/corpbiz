<?php get_header(); ?>
<!-- Page Section -->
<div class="page_mycarousel">
	<div class="container page_title_col">
		<div class="row">
			<div class="hc_page_header_area">
				<h1><?php echo single_cat_title("", false); ?></h1>
			</div>
		</div>
	</div>
</div>
<!-- /Page Section -->
<!-- Blog & Sidebar Section -->
<div class="container">
	<div class="row blog_sidebar_section">
		<!--Blog-->
		<div class="<?php corpbiz_post_layout_class(); ?>" >		
		<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$category_id=get_query_var('cat');
				$args = array( 'post_type' => 'post','paged'=>$paged,'cat' => $category_id);		
				$post_type_data = new WP_Query( $args );
					while($post_type_data->have_posts()):
					$post_type_data->the_post();
					global $more;
					$more = 0; 
					get_template_part('content','');
					endwhile; 
					$Webriti_pagination = new Webriti_pagination();
					$Webriti_pagination->Webriti_page($paged, $post_type_data);					
			?>
		</div>
		<?php get_sidebar(); ?>
	</div>
</div>	
<?php get_footer(); ?>