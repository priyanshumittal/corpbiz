<?php $current_options = get_option('appointment_options',theme_data_setup());  ?>
<div class="container">
		<!--Portfolio Tabs-->
		<div class="row">
		<?php
		//for a given post type, return all
		$post_type = 'corpbiz_portfolio';
		$tax = 'cor_portfolio_categories'; 
		$term_args=array( 'hide_empty' => true);
		$tax_terms = get_terms($tax, $term_args);
		$defualt_tex_id = get_option('custom_texo_corpbiz');
		if($tax_terms) {
		?>	
		<div class="col-md-12 portfolio_tabs_section">
			<ul class="portfolio_tabs" id="mytabs">
				<span><?php _e('Filter :','corpbiz'); ?></span>
				<?php	foreach ($tax_terms  as $tax_term) { 
				$tax_term_name = str_replace(' ', '_', $tax_term->name);
				$tax_term_name = preg_replace('~[^A-Za-z\d\s-]+~u', 'qua', $tax_term_name);
				?>
				<li <?php if($tax_term->term_id == $defualt_tex_id) echo "class='active'"; ?>><a data-toggle="tab" href="#<?php echo $tax_term_name; ?>"><?php echo $tax_term->name; ?></a></li>
				<?php } ?>				
			</ul>
		</div>
		<?php } else { ?>
			<div class="col-md-12">
				<div class="error_404">
					<h2><?php _e('No Portfolio available','corpbiz'); ?></h2>
					<h4><?php _e('Please Add your portfolio.','corpbiz'); ?></h4>
					<p><?php _e('Using Admin Dashboard -> Portfolio Menu.','corpbiz'); ?></p>
				</div>
			</div>
		<?php } ?>
	</div>
		<div class="tab-content main-portfolio-section" id="">
		<?php 
		global $paged;
		$curpage = $paged ? $paged : 1;
		
		$norecord=0;
		$total_posts=0;
		$min_post_start=0;
		$is_active=true;
		if ($tax_terms) 
		{ 	foreach ($tax_terms  as $tax_term)
			{	
				if(isset($_POST['total_posts']))
				{
					$count_posts = $_POST['total_posts'];
				}
				else
				{
					//$count_posts = wp_count_posts( $post_type)->publish; 
					if(is_page_template('portfolio-2-col.php')) {$count_posts = 4;}
					if(is_page_template('portfolio-3-col.php')) {$count_posts = 6;}
					if(is_page_template('portfolio-4-col.php')) {$count_posts = 8;}
					
				}
				if(isset($_POST['min_post_start']))
				{
					$min_post_start = $_POST['min_post_start'];
				}
				$count_posts = wp_count_posts( $post_type)->publish; 
				$args = array (
				'post_type' => $post_type,
				'cor_portfolio_categories' => $tax_term->slug,
				'posts_per_page' =>$count_posts,
				'post_status' => 'publish');
				$portfolio_query = null;		
				$portfolio_query = new WP_Query($args);
				
				
				
			global $j;
			$j=1;
			$portfolio_query = null;		
			$portfolio_query = new WP_Query($args);	
			if( $portfolio_query->have_posts() )
			{ 
				$tax_term_name = str_replace(' ', '_', $tax_term->name);
				$tax_term_name = preg_replace('~[^A-Za-z\d\s-]+~u', 'qua', $tax_term_name); 
			?>
			<div id="<?php echo $tax_term->slug; ?>" class="tab-pane fade in <?php if($tab==''){if($is_active==true){echo 'active';}$is_active=false;}else if($tab==$tax_term->slug){echo 'active';} ?>">
			<div class="row">
				<?php
					while ($portfolio_query->have_posts()) { $portfolio_query->the_post();
					if(get_post_meta( get_the_ID(),'project_more_btn_link', true )) 
					{ $project_more_btn_link = get_post_meta( get_the_ID(),'project_more_btn_link', true );
					} else {
					$project_more_btn_link = '';
					} ?>
					
					
					<?php if(is_page_template('portfolio-2-col.php')) { ?>
					<div class="col-md-6 col-md-6 portfolio-area">
					<?php } ?>
					
					<?php if(is_page_template('portfolio-3-col.php')) { ?>
					<div class="col-md-4 col-md-6 portfolio-area">
					<?php } ?>
					
					<?php if(is_page_template('portfolio-4-col.php')) { ?>
					<div class="col-md-3 col-md-6 portfolio-area">
					<?php } ?>
						
						<div class="portfolio-image">
							<?php if(is_page_template('portfolio-2-col.php')) { 
							appointment_image_thumbnail('','img-responsive');
							} ?>
							
							<?php if(is_page_template('portfolio-3-col.php')) {
							appointment_image_thumbnail('','img-responsive');
							} ?>
							
							<?php if(is_page_template('portfolio-4-col.php')) { 
							appointment_image_thumbnail('','img-responsive');
							} ?>

							<?php
							if(has_post_thumbnail())
							{ 
							$post_thumbnail_id = get_post_thumbnail_id();
							$post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id );
							} ?>
							<div class="portfolio-showcase-overlay">
								<div class="portfolio-showcase-overlay-inner">
									<div class="portfolio-showcase-icons">
									<?php if(isset($post_thumbnail_url)){ ?>
									<a href="<?php echo $post_thumbnail_url; ?>"  data-lightbox="image" title="<?php the_title(); ?>" class="hover_thumb"><i class="fa fa-search"></i></a>
									<?php } ?>
									<?php if(!empty($project_more_btn_link)) {?>
									<a href="<?php echo $project_more_btn_link;?>" <?php if(!empty($project_link_chkbx)){ echo 'target="_blank"'; } ?>  title="Appointment" class="hover_thumb"><i class="fa fa-plus"></i></a>
									<?php } ?>
									</div>
								</div>
							</div>
							</div>
						<div class="portfolio-caption">
							<h4>
							<?php
							
							appointment_get_custom_link($project_more_btn_link,get_post_meta( get_the_ID(), 'project_more_btn_target', true ),get_the_title()); ?>
							</h4>
							<?php if(get_post_meta( get_the_ID(), 'project_description', true )){ ?>
							<p><?php echo get_post_meta( get_the_ID(), 'project_description', true ); ?></p>
							<?php } ?>
						</div>
						
					 </div>
					 <?php 
		// call clearfix css class		
	    appointment_portfolio_clearfix($j);  
		
			$norecord=1; } ?>
			</div>
		
		<?php $count_posts_2c = wp_count_posts('appoint_portfolio')->publish;
			if($count_posts_2c > 4) {
				$Webriti_pagination = new Webriti_pagination();
				$Webriti_pagination->Webriti_page($curpage, $portfolio_query);
			}
			else if($count_posts_2c > 6)
			{
			$Webriti_pagination = new Webriti_pagination();
			$Webriti_pagination->Webriti_page($curpage, $portfolio_query);
			}
			else if($count_posts_2c > 6)
			{
			$Webriti_pagination = new Webriti_pagination();
			$Webriti_pagination->Webriti_page($curpage, $portfolio_query);
			}
		?>
		</div>
			<?php
			}else{
			?>
			<div id="<?php echo $tax_term->slug; ?>" class="tab-pane fade in <?php if($tab==''){if($is_active==true){echo 'active';}$is_active=false;}else if($tab==$tax_term->slug){echo 'active';} ?>"></div>
			<?php
			}
		  } 
		}  
		?>
		<?php wp_reset_query(); ?>
		
			</div>
			<!-- /Load More Projects Btn -->			
			</div>
		</div>
		<!-- /Portfolio Area1 -->
						
	</div>
</div>
<!-- /Portfolio Section -->
<?php get_footer(); ?>