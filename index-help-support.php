<!--Homepage Support Section-->
<?php  $current_options = get_option('corpbiz_options',theme_data_setup()); ?>
<div class="support_section" <?php if($current_options['home_theme_support_bg']!='') { ?> style="background: url('<?php echo $current_options['home_theme_support_bg']; ?>') no-repeat fixed 0 0 / cover rgba(0, 0, 0, 0);" <?php } ?> >
	<div class="container">
		<div class="row">
			<div class="support_heading_title">
			<?php if($current_options['home_theme_support_title']!='') { ?>
				<h1><?php echo $current_options['home_theme_support_title']; ?></h1>
				<?php } 
				if($current_options['home_theme_support_description']!='') { ?>
				<p><?php echo $current_options['home_theme_support_description']; ?> </p>
				<?php } ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
				<?php if($current_options['home_support_icon_one']!='') { ?>	
				<div class="support_icon_box support_icon_red">
					<i class="fa <?php echo $current_options['home_support_icon_one']; ?>"></i>
				</div>
				<?php } ?>
				<div class="support_content_box">
				<?php if($current_options['home_support_title_one']!='') { ?>	
					<h4><?php echo $current_options['home_support_title_one']; ?></h4>
				<?php } 
					if($current_options['home_support_desciption_one']!='') { ?>
					<p><?php echo $current_options['home_support_desciption_one']; ?></p>
				<?php } ?>
				</div>
				<?php if($current_options['home_support_learn_more_text_one']!='') { ?>
				<div class="support_content_btn">
					<a <?php if($current_options['home_support_learn_more_target_one']=='on') { echo "target='_blank'"; } ?> class="support_btn_red" href="<?php if($current_options['home_support_learn_more_link_one']!='') { echo $current_options['home_support_learn_more_link_one']; } ?>">
						<?php echo $current_options['home_support_learn_more_text_one']; ?>
					</a>
				</div>
				<?php } ?>
			</div>
			
			<div class="col-md-4">
				<?php if($current_options['home_support_icon_two']!='') { ?>	
				<div class="support_icon_box support_icon_orange">
					<i class="fa <?php echo $current_options['home_support_icon_two']; ?>"></i>
				</div>
				<?php } ?>
				<div class="support_content_box">
				<?php if($current_options['home_support_title_two']!='') { ?>	
					<h4><?php echo $current_options['home_support_title_two']; ?></h4>
				<?php } 
					if($current_options['home_support_desciption_two']!='') { ?>
					<p><?php echo $current_options['home_support_desciption_two']; ?></p>
				<?php } ?>
				</div>
				<?php if($current_options['home_support_learn_more_text_two']!='') { ?>
				<div  class="support_content_btn">
					<a <?php if($current_options['home_support_learn_more_target_two']=='on') { echo "target='_blank'"; } ?> class="support_btn_orange" href="<?php if($current_options['home_support_learn_more_link_two']!='') { echo $current_options['home_support_learn_more_link_two']; } ?>">
						<?php echo $current_options['home_support_learn_more_text_two']; ?>
					</a>
				</div>
				<?php } ?>
			</div>			
			<div class="col-md-4">
				<?php if($current_options['home_support_icon_three']!='') { ?>	
				<div class="support_icon_box support_icon_blue">
					<i class="fa <?php echo $current_options['home_support_icon_three']; ?>"></i>
				</div>
				<?php } ?>
				<div class="support_content_box">
				<?php if($current_options['home_support_title_three']!='') { ?>	
					<h4><?php echo $current_options['home_support_title_three']; ?></h4>
				<?php } 
					if($current_options['home_support_desciption_three']!='') { ?>
					<p><?php echo $current_options['home_support_desciption_three']; ?></p>
				<?php } ?>
				</div>
				<?php if($current_options['home_support_learn_more_text_three']!='') { ?>
				<div class="support_content_btn">
					<a <?php if($current_options['home_support_learn_more_target_three']=='on') { echo "target='_blank'"; } ?> class="support_btn_blue" href="<?php if($current_options['home_support_learn_more_link_three']!='') { echo $current_options['home_support_learn_more_link_three']; } ?>">
						<?php echo $current_options['home_support_learn_more_text_three']; ?>
					</a>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<!--/Homepage Support Section-->