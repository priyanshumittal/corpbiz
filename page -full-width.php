<?php // Template Name: Page-full-width 
get_header(); ?>

<!-- Page Section -->
<div class="page_mycarousel" <?php if(get_post_meta( get_the_ID(), 'page_header_image', true )) { ?> style="background: url('<?php echo get_post_meta( get_the_ID(), 'page_header_image', true ); ?>')  repeat scroll 0 0 / cover;" <?php } ?> >
	<div class="container page_title_col">
		<div class="row">
			<div class="hc_page_header_area">
				<h1> <?php the_title(); ?> </h1>		
			</div>
		</div>
	</div>
</div>
<!-- /Page Section -->

<!-- Blog & Sidebar Section -->
<div class="container">
	<div class="row blog_sidebar_section">		
		<!--Blog-->
<div class="col-md-12">
	<?php the_post(); ?>
	<div class="blog_detail_section">
			<?php if(has_post_thumbnail()): ?>
			<?php $defalt_arg =array('class' => "img-responsive"); ?>
			<div class="blog_post_img">
				<?php the_post_thumbnail('', $defalt_arg); ?>	
			</div>
			<?php endif; ?>
			<!--<div class="post_title_wrapper">
				<h2><a href="<?php /*the_permalink(); ?>"><?php //the_title(); ?></a></h2>
				<div class="post_detail">
					<a href="#"><i class="fa fa-calendar"></i> <?php echo get_the_date('M j, Y'); ?> </a>
					<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><i class="fa fa-user"></i> <?php _e('Posted by : &nbsp;', 'corpbiz'); ?> <?php the_author(); ?> </a>
					<a href="<?php comments_link(); ?>"><i class="fa fa-comments"></i> <?php comments_number('No Comments', '1 Comment','% Comments'); ?></a>
					<?php 	$tag_list = get_the_tag_list();
							if(!empty($tag_list)) { ?>
					<div class="post_tags">
						<i class="fa fa-tags"></i><?php the_tags('', ',', '<br />'); ?>
					</div>
					<?php } */ ?>
				</div>
			</div> -->
			<div class="blog_post_content">
				<?php the_content(); ?>
			</div>	
	</div>			
	<?php comments_template('',true); ?>	
	</div>
</div>
</div>
<!--Blog-->


<?php get_footer(); ?>