<?php
/************ Home slider meta post ****************/
add_action('admin_init','corpbiz_init');
function corpbiz_init()
	{
		add_meta_box('home_slider_meta', 'Description', 'corpbiz_meta_slider', 'corpbiz_slider', 'normal', 'high');
		add_meta_box('home_service_meta', 'featured Service', 'corpbiz_meta_service', 'corpbiz_service', 'normal', 'high');
		add_meta_box('home_portfolio_meta', 'Custom Portfolio Details', 'corpbiz_meta_portfolio', 'corpbiz_portfolio', 'normal', 'high');
		add_meta_box('home_project_meta_details', 'Project Featured Details', 'corpbiz_meta_project_details', 'corpbiz_project', 'normal', 'high');
		add_meta_box('corpbiz_team', 'Detail', 'corpbiz_meta_team', 'corpbiz_team', 'normal', 'high');
		add_meta_box('corpbiz_testimonilas', 'Description', 'corpbiz_meta_testimonial', 'corpbiz_testimonial', 'normal', 'high');
		add_meta_box('corpbiz_page', 'Page Header Background Image', 'page_background_image_meta', 'page', 'normal', 'high');
		add_action('save_post','corpbiz_meta_save');
	}
	function page_background_image_meta() {
		wp_enqueue_script('media-upload', array('media-upload'));	
		wp_enqueue_style('thickbox');
		?>
		<script>
		 jQuery(document).ready(function() {
		 // media upload js
				var uploadID = ''; /*setup the var*/
				jQuery('.upload_image_button').click(function() {
					uploadID = jQuery(this).prev('input'); /*grab the specific input*/
					
					formfield = jQuery('.upload').attr('name');
					tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
					
					window.send_to_editor = function(html)
					{
						imgurl = jQuery('img',html).attr('src');
						uploadID.val(imgurl); /*assign the value to the input*/
						tb_remove();
					};		
					return false;
				});	
				
			});
		</script>
		<?php 
		global $post ;
		$page_header_image = sanitize_text_field( get_post_meta( get_the_ID(), 'page_header_image', true ));
		?>		
		<p><label><?php _e('Page Header Background color','corpbiz');?></label></p> 
		<input class="webriti_inpute" type="text" value="<?php echo $page_header_image;   ?>" id="page_header_image" name="page_header_image" size="36" class="upload has-file"/>
		<input type="button" id="upload_button" value="Upload header image" class="upload_image_button" />
		<?php
		
	}
	
	// code for service description
	function corpbiz_meta_service()
	{	global $post ;
		
		$service_icon_image =sanitize_text_field( get_post_meta( get_the_ID(), 'service_icon_image', true ));
		$meta_service_link =sanitize_text_field( get_post_meta( get_the_ID(), 'meta_service_link', true ));
		$meta_service_target =sanitize_text_field( get_post_meta( get_the_ID(), 'meta_service_target', true ));
		$service_description_text =sanitize_text_field( get_post_meta( get_the_ID(), 'service_description_text', true ));
	?>	
		<p><h4 class="heading"><?php _e('Service Icon (Using Font Awesome icons name) like: fa-rub.','corpbiz');?> <label style="margin-left:10px;"><a target="_blank" href="http://fontawesome.io/icons/"> <?php _e('Get your fontawesome icons.','corpbiz') ;?></a></label></h4>
		<p><input class="inputwidth"  name="service_icon_image" id="service_icon_image" style="width: 480px" placeholder="Enter the fontawesome icon" type="text" value="<?php if (!empty($service_icon_image)) echo esc_attr($service_icon_image);?>"> </input></p>	
		<span style="font-size:14px; font-weight:bold;"><?php _e('For adding colors to these service icons we have provided four color classes as follows','corpbiz'); ?> <br>
		(1) <?php _e('color_red','corpbiz'); ?> &nbsp; (2) <?php _e('color_blue','corpbiz'); ?> &nbsp; (3) <?php _e('color_green','corpbiz'); ?> &nbsp; (4) <?php _e('color_orange','corpbiz'); ?><br>
		<?php _e('For example : fa-wordpress color_red','corpbiz'); ?> </span>
		<p><h4 class="heading"><?php _e('Service Link','corpbiz'); ?></h4>
		<p><input type="checkbox" id="meta_service_target" name="meta_service_target" <?php if($meta_service_target) echo "checked"; ?> ><?php _e('Open link in a new window/tab','corpbiz'); ?></p>
		<p><input class="inputwidth"  name="meta_service_link" id="meta_service_link" style="width: 480px" placeholder="Enter the service link" type="text" value="<?php if (!empty($meta_service_link)) echo esc_attr($meta_service_link);?>"> </input></p>
		<p><h4 class="heading"><?php _e('Service Description','corpbiz'); ?></h4>
		<p><textarea name="service_description_text" id="service_description_text" style="width: 480px; height: 56px; padding: 0px;" placeholder="Enter Description for your service"  rows="3" cols="10" ><?php if (!empty($service_description_text)) echo esc_textarea( $service_description_text ); ?></textarea></p>
<?php }
	
// code for project description
	function corpbiz_meta_portfolio()
	{	global $post ;		
		$portfolio_summary =sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_summary', true ));
		$meta_portfolio_link =sanitize_text_field( get_post_meta( get_the_ID(), 'meta_portfolio_link', true ));
		$meta_portfolio_target =sanitize_text_field( get_post_meta( get_the_ID(), 'meta_portfolio_target', true ));
		
	?>
	<p><h4 class="heading"><?php _e('Portfolio Description','corpbiz');?></h4>
	<p><input class="inputwidth"  name="portfolio_summary" id="portfolio_summary" style="width: 480px" placeholder="Enter the  portfolio description" type="text" value="<?php if (!empty($portfolio_summary)) echo esc_attr($portfolio_summary);?>"> </input></p>	
	<p><h4 class="heading"><?php _e('Portfolio Link','corpbiz'); ?></h4>
	<p><input type="checkbox" id="meta_portfolio_target" name="meta_portfolio_target" <?php if($meta_portfolio_target) echo "checked"; ?> ><?php _e('Open link in a new window/tab','corpbiz'); ?></p>
	<p><input class="inputwidth"  name="meta_portfolio_link" id="meta_portfolio_link" style="width: 480px" placeholder="Enter the portfolio link" type="text" value="<?php if (!empty($meta_portfolio_link)) echo esc_attr($meta_portfolio_link);?>"> </input></p>
		
<?php }
	function corpbiz_meta_project_details()
	{	global $post ;
		$project_title =sanitize_text_field( get_post_meta( get_the_ID(), 'project_title', true ));
		$project_desciption =sanitize_text_field( get_post_meta( get_the_ID(), 'project_desciption', true ));
		$project_button_text =sanitize_text_field( get_post_meta( get_the_ID(), 'project_button_text', true ));
		$meta_button_link =sanitize_text_field( get_post_meta( get_the_ID(), 'meta_button_link', true ));
		$meta_button_target =sanitize_text_field( get_post_meta( get_the_ID(), 'meta_button_target', true ));
	?>
	<p><h4 class="heading"><?php _e('Project title','corpbiz');?></h4>
	<p><input class="inputwidth"  name="project_title" id="project_title" style="width: 480px" placeholder="Enter the project title" type="text" value="<?php if (!empty($project_title)) echo esc_attr($project_title);?>"> </input></p>	
	<p><h4 class="heading"><?php _e('Project description ','corpbiz');?></h4>
	<p><input class="inputwidth"  name="project_desciption" id="project_desciption" style="width: 480px" placeholder="Enter the project description" type="text" value="<?php if (!empty($project_desciption)) echo esc_attr($project_desciption);?>"> </input></p>
	<p><h4 class="heading"><?php _e('Button Text','corpbiz');?></h4>
	<p><input class="inputwidth"  name="project_button_text" id="project_button_text" style="width: 480px" placeholder="Enter the button text" type="text" value="<?php if (!empty($project_button_text)) echo esc_attr($project_button_text);?>"> </input></p>	
	<p><h4 class="heading"><?php _e('Button Link','corpbiz');?></h4>
	<p><input type="checkbox" id="meta_button_target" name="meta_button_target" <?php if($meta_button_target) echo "checked"; ?> ><?php _e('Open link in a new window/tab','corpbiz'); ?></p>
	<p><input class="inputwidth"  name="meta_button_link" id="meta_button_link" style="width: 480px" placeholder="Enter the button link" type="text" value="<?php if (!empty($meta_button_link)) echo esc_attr($meta_button_link);?>"> </input></p>	
	
	<?php		
	}
	
	//Meta boxes for testimonials*/
	function corpbiz_meta_testimonial()
	{	global $post ;
		$testimonial_description_meta=sanitize_text_field( get_post_meta( get_the_ID(), 'testimonial_description_meta', true ));
		$testimonial_author_designation_meta=sanitize_text_field( get_post_meta( get_the_ID(), 'testimonial_author_designation_meta', true ));	
		
		$testimonial_link_meta=sanitize_text_field( get_post_meta( get_the_ID(), 'testimonial_link_meta', true ));
		$testimonial_link_target_meta =sanitize_text_field( get_post_meta( get_the_ID(), 'testimonial_link_target_meta', true ));
		
		?>	
		<p><label><?php _e('Testimonial Description','corpbiz');?></label>	</p>
		<p><textarea name="testimonial_description_meta" id="testimonial_description_meta" style="width: 480px; height: 56px; padding: 0px;" placeholder="Enter Desc for your Testimonial"  rows="3" cols="10" ><?php if (!empty($testimonial_description_meta)) echo esc_textarea( $testimonial_description_meta ); ?></textarea></p>
		
		<p><label><?php _e('Testimonial Author Designation','corpbiz');?></label></p> 
		<p><input class="inputwidth" name="testimonial_author_designation_meta" id="testimonial_author_designation_meta" style="width: 480px;" placeholder="Author Designation"	type="text" value="<?php if (!empty($testimonial_author_designation_meta)) echo esc_attr($testimonial_author_designation_meta);?>"></input></p>
		
		<p><h4 class="heading"><?php _e('Testimonial Link','corpbiz');?></h4>
		<p><input type="checkbox" id="testimonial_link_target_meta" name="testimonial_link_target_meta" <?php if($testimonial_link_target_meta) echo "checked"; ?> ><?php _e('Open link in a new window/tab','corpbiz'); ?></p>
		<p><input class="inputwidth"  name="testimonial_link_meta" id="testimonial_link_meta" style="width: 480px" placeholder="Enter the testimonial link" type="text" value="<?php if (!empty($testimonial_link_meta)) echo esc_attr($testimonial_link_meta);?>"> </input></p>	
	
	<?php
	}
	
	function corpbiz_meta_team()
	{ 
		global $post;
		
		$description_meta_save = sanitize_text_field( get_post_meta( get_the_ID(), 'description_meta_save', true ));
		$fb_meta_save = sanitize_text_field( get_post_meta( get_the_ID(), 'fb_meta_save', true ));
		$fb_meta_save_chkbx = sanitize_text_field( get_post_meta( get_the_ID(), 'fb_meta_save_chkbx', true ));
		$twt_meta_save = sanitize_text_field( get_post_meta( get_the_ID(), 'twt_meta_save', true ));
		$twt_meta_save_chkbx = sanitize_text_field( get_post_meta( get_the_ID(), 'twt_meta_save_chkbx', true ));
		$flickr_meta_save = sanitize_text_field( get_post_meta( get_the_ID(), 'flickr_meta_save', true ));
		$flickr_meta_save_chkbx = sanitize_text_field( get_post_meta( get_the_ID(), 'flickr_meta_save_chkbx', true ));
		$google_meta_save = sanitize_text_field( get_post_meta( get_the_ID(), 'google_meta_save', true ));
		$google_meta_save_chkbx = sanitize_text_field( get_post_meta( get_the_ID(), 'google_meta_save_chkbx', true ));
	?>	
	<p><h4 class="heading"><?php _e('Role Description','corpbiz');?></h4></p>
	<p><textarea name="description_meta_save" id="description_meta_save" style="width: 480px; height: 56px; padding: 0px;" placeholder="Describe the Roles for the member(Use max-word 140)"  rows="3" cols="10" ><?php if (!empty($description_meta_save)) echo esc_textarea( $description_meta_save ); ?></textarea></p>	
	<p><h4 class="heading"><span><?php _e('Social Media Setting','corpbiz');?></span></h4>
	<p><h4 class="heading"><label><?php _e('Facebook','corpbiz');?></label></h4>
	<input style="width:80%;padding: 10px;"  name="fb_meta_save" id="fb_meta_save" placeholder="Enter Your Fb's URL in https formate" value="<?php if(!empty($fb_meta_save)) echo esc_attr($fb_meta_save); ?>"/>
	<input type="checkbox" name="fb_meta_save_chkbx" value="1"<?php if(isset($fb_meta_save_chkbx)) checked($fb_meta_save_chkbx,'1') ; ?> /></p>
	<p><h4 class="heading"><?php _e('Twitter Url','corpbiz')?></h4>	 
	 <p><input style="width:80%; padding: 10px;"  name="twt_meta_save" id="twt_meta_save" placeholder="Enter Your Twitter's URL in https formate"  value="<?php if(!empty($twt_meta_save)) echo esc_attr($twt_meta_save); ?>"/>	
	<input type="checkbox" name="twt_meta_save_chkbx" value="1"<?php if(isset($twt_meta_save_chkbx)) checked($twt_meta_save_chkbx,'1') ; ?> /></p>
	
	<p><h4 class="heading"><label><?php _e('Flickr Url','corpbiz');?></label></h4>
	<input style="width:80%;padding: 10px;"  name="flickr_meta_save" id="flickr_meta_save" placeholder="Enter Your Flickr's URL in https formate" value="<?php if(!empty($flickr_meta_save)) echo esc_attr($flickr_meta_save); ?>"/>
	<input type="checkbox" name="flickr_meta_save_chkbx" value="1" <?php if(isset($flickr_meta_save_chkbx)) checked($flickr_meta_save_chkbx,'1') ; ?> /></p>
	
	<p><h4 class="heading"><label><?php _e('Google Url','corpbiz');?></label></h4>
	<input style="width:80%; padding: 10px;"  name="google_meta_save" id="google_meta_save" placeholder="Enter Your Google's URL in https formate" value="<?php if(!empty($google_meta_save)) echo esc_attr($google_meta_save); ?>"/>
	<input type="checkbox" name="google_meta_save_chkbx" value="1" <?php if(isset($google_meta_save_chkbx)) checked($google_meta_save_chkbx,'1') ; ?> /></p>
	
	<?php
	}

function corpbiz_meta_save($post_id) 
{	
	if((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit']))
        return;
		
	if ( ! current_user_can( 'edit_page', $post_id ) )
	{     return ;	} 		
	if(isset($_POST['post_ID']))
	{ 	
		$post_ID = $_POST['post_ID'];				
		$post_type=get_post_type($post_ID);
		if($post_type=='corpbiz_slider'){ 		
			update_post_meta($post_ID, 'slider_description', sanitize_text_field($_POST['slider_description']));
		} 
		else if($post_type=='corpbiz_service'){		
			update_post_meta($post_ID, 'service_icon_image', sanitize_text_field($_POST['service_icon_image']));				
			update_post_meta($post_ID, 'meta_service_link', sanitize_text_field($_POST['meta_service_link']));
			update_post_meta($post_ID, 'meta_service_target', sanitize_text_field($_POST['meta_service_target']));
			update_post_meta($post_ID, 'service_description_text', sanitize_text_field($_POST['service_description_text']));
		}		
		else if($post_type=='corpbiz_portfolio'){			
			update_post_meta($post_ID, 'meta_portfolio_target', sanitize_text_field($_POST['meta_portfolio_target']));	
			update_post_meta($post_ID, 'meta_portfolio_link', sanitize_text_field($_POST['meta_portfolio_link']));	
			update_post_meta($post_ID, 'portfolio_summary', sanitize_text_field($_POST['portfolio_summary']));	
		}
		else if($post_type=='corpbiz_project'){			
			update_post_meta($post_ID, 'project_title', sanitize_text_field($_POST['project_title']));
			update_post_meta($post_ID, 'project_desciption', sanitize_text_field($_POST['project_desciption']));
			update_post_meta($post_ID, 'project_button_text', sanitize_text_field($_POST['project_button_text']));
			update_post_meta($post_ID, 'meta_button_link', sanitize_text_field($_POST['meta_button_link']));
			update_post_meta($post_ID, 'meta_button_target', sanitize_text_field($_POST['meta_button_target']));
		}
		else if($post_type=='corpbiz_testimonial') {		
			update_post_meta($post_ID, 'testimonial_description_meta', sanitize_text_field($_POST['testimonial_description_meta']));
			update_post_meta($post_ID, 'testimonial_author_designation_meta', sanitize_text_field($_POST['testimonial_author_designation_meta']));
			update_post_meta($post_ID, 'testimonial_link_meta', sanitize_text_field($_POST['testimonial_link_meta']));
			update_post_meta($post_ID, 'testimonial_link_target_meta', sanitize_text_field($_POST['testimonial_link_target_meta']));
			
		}
		else if($post_type=='corpbiz_team') {			
			update_post_meta($post_ID, 'description_meta_save', sanitize_text_field($_POST['description_meta_save']));
			update_post_meta($post_ID, 'fb_meta_save', sanitize_text_field($_POST['fb_meta_save']));
			update_post_meta($post_ID, 'fb_meta_save_chkbx', sanitize_text_field($_POST['fb_meta_save_chkbx']));
			update_post_meta($post_ID, 'twt_meta_save', sanitize_text_field($_POST['twt_meta_save']));
			update_post_meta($post_ID, 'twt_meta_save_chkbx', sanitize_text_field($_POST['twt_meta_save_chkbx']));
			update_post_meta($post_ID, 'flickr_meta_save', sanitize_text_field($_POST['flickr_meta_save']));
			update_post_meta($post_ID, 'flickr_meta_save_chkbx', sanitize_text_field($_POST['flickr_meta_save_chkbx']));
			update_post_meta($post_ID, 'google_meta_save', sanitize_text_field($_POST['google_meta_save']));
			update_post_meta($post_ID, 'google_meta_save_chkbx', sanitize_text_field($_POST['google_meta_save_chkbx']));
		}
		else if($post_type=='page')
		{
			update_post_meta($post_ID, 'page_header_image', sanitize_text_field($_POST['page_header_image']));
		
		}
	}			
} 
?>