<div class="block ui-tabs-panel deactive" id="option-ui-id-7" >	
	<?php $current_options = wp_parse_args(  get_option( 'corpbiz_options', array() ), theme_data_setup() );
	if(isset($_POST['webriti_settings_save_7']))
	{	
		if($_POST['webriti_settings_save_7'] == 1) 
		{
			if ( empty($_POST) || !wp_verify_nonce($_POST['webriti_gernalsetting_nonce_customization'],'webriti_customization_nonce_gernalsetting') )
			{  print 'Sorry, your nonce did not verify.';	exit; }
			else  
			{		
				
				$current_options['send_usmessage']=sanitize_text_field($_POST['send_usmessage']);
				$current_options['contact_address']=sanitize_text_field($_POST['contact_address']);
				$current_options['contact_address_two']=sanitize_text_field($_POST['contact_address_two']);
				$current_options['contact_phone_number']=sanitize_text_field($_POST['contact_phone_number']);
				$current_options['contact_email']=sanitize_text_field($_POST['contact_email']);
				
				update_option('corpbiz_options', stripslashes_deep($current_options));
			}
		}	
		if($_POST['webriti_settings_save_7'] == 2) 
		{	
			$current_options['send_usmessage']="Send Us a Message";	
			$current_options['contact_address']="138, AtlantisLnKingsport"; 
			$current_options['contact_address_two']="Illinois. 121164";
			$current_options['contact_phone_number']="420-300-3850";
			$current_options['contact_email']="info@hctheme.com";
			
			
			update_option('corpbiz_options', $current_options);
		}
	}  ?>
	<form method="post" id="webriti_theme_options_7">
		<div id="heading">
			<table style="width:100%;"><tr>
				<td><h2><?php _e('Contact Information','corpbiz');?></h2></td>
				<td style="width:30%;">
					<div class="webriti_settings_loding" id="webriti_loding_7_image"></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_7_success" ><?php _e('Options data successfully Saved','corpbiz');?></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_7_reset" ><?php _e('Options data successfully reset','corpbiz');?></div>
				</td>
				<td style="text-align:right;">
					<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('7');">
					<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('7')" >
				</td>
				</tr>
			</table>	
		</div>	
		
		<?php wp_nonce_field('webriti_customization_nonce_gernalsetting','webriti_gernalsetting_nonce_customization'); ?>
		
		<div class="section">
			<h3><?php _e('Contact Us Text:','corpbiz');?></h3>
			<input class="webriti_inpute"  type="text" name="send_usmessage" id="send_usmessage" value="<?php if($current_options['send_usmessage']!='') { echo esc_attr($current_options['send_usmessage']); } ?>" >
			<span class="explain"><?php  _e('Enter Contact Us Text.','corpbiz');?></span>
		</div>		
		<div class="section">
			<h3><?php _e('Contact Address Line One:','corpbiz');?></h3>
			<input class="webriti_inpute"  type="text" name="contact_address" id="contact_address" value="<?php if($current_options['contact_address']!='') { echo esc_attr($current_options['contact_address']); } ?>" >
			<span class="explain"><?php  _e('Enter Contact address.','corpbiz');?></span>
		</div>
		
		<div class="section">
			<h3><?php _e('Contact Address Line Two:','corpbiz');?></h3>
			<input class="webriti_inpute"  type="text" name="contact_address_two" id="contact_address_two" value="<?php if($current_options['contact_address_two']!='') { echo esc_attr($current_options['contact_address_two']); } ?>" >
			<span class="explain"><?php  _e('Enter Contact address.','corpbiz');?></span>
		</div>
		
		<div class="section">
			<h3><?php _e('Contact Phone Number:','corpbiz');?></h3>
			<input class="webriti_inpute"  type="text" name="contact_phone_number" id="contact_phone_number" value="<?php if($current_options['contact_phone_number']!='') { echo esc_attr($current_options['contact_phone_number']); } ?>" >
			<span class="explain"><?php  _e('Enter Contact phone number.','corpbiz');?></span>
		</div>		
		
		<div class="section">
			<h3><?php _e('Contact Email:','corpbiz');?></h3>
			<input class="webriti_inpute"  type="text" name="contact_email" id="contact_email" value="<?php if($current_options['contact_email']!='') { echo esc_attr($current_options['contact_email']); } ?>" >
			<span class="explain"><?php  _e('Enter Contact email address.','corpbiz');?></span>
		</div>
			
		
		<div id="button_section">
			<input type="hidden" value="1" id="webriti_settings_save_7" name="webriti_settings_save_7" />
			<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('7');">
			<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('7')" >
		</div>
	</form>
</div>