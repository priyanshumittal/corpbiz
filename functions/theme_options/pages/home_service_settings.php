<div class="block ui-tabs-panel deactive" id="option-ui-id-14" >	
	<?php $current_options = wp_parse_args(  get_option( 'corpbiz_options', array() ), theme_data_setup() );
	if(isset($_POST['webriti_settings_save_14']))
	{	
		if($_POST['webriti_settings_save_14'] == 1) 
		{
			if ( empty($_POST) || !wp_verify_nonce($_POST['webriti_gernalsetting_nonce_customization'],'webriti_customization_nonce_gernalsetting') )
			{  print 'Sorry, your nonce did not verify.';	exit; }
			else  
			{	
				$current_options['service_list']= sanitize_text_field($_POST['service_list']);
				$current_options['home_service_title'] = sanitize_text_field($_POST['home_service_title']);
				$current_options['home_service_description']= sanitize_text_field($_POST['home_service_description']);
				update_option('corpbiz_options', stripslashes_deep($current_options));
			}
		}	
		if($_POST['webriti_settings_save_14'] == 2) 
		{
			$current_options['service_list']=4;
			$current_options['home_service_title'] ='Our Nice Services';
			$current_options['home_service_description'] ='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam scelerisque faucibus risus non iaculis.';
			update_option('corpbiz_options',$current_options);
		}
	}  ?>
	<form method="post" id="webriti_theme_options_14">
		<div id="heading">
			<table style="width:100%;"><tr>
				<td><h2><?php _e('Service Settings ','corpbiz');?></h2></td>
				<td><div class="webriti_settings_loding" id="webriti_loding_14_image"></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_14_success" ><?php _e('Options data successfully Saved','corpbiz');?></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_14_reset" ><?php _e('Options data successfully reset','corpbiz');?></div>
				</td>
				<td style="text-align:right;">
					<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('14');">
					<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('14')" >
				</td>
				</tr>
			</table>	
		</div>		
		<?php wp_nonce_field('webriti_customization_nonce_gernalsetting','webriti_gernalsetting_nonce_customization'); ?>
		<div class="section">
		<h3><?php _e('Services Section','quality'); ?></h3>
		<hr>
			<h3><?php _e('Number of services on service section','quality'); ?></h3>
			<?php $service_list = $current_options['service_list']; ?>		
			<select name="service_list" class="webriti_inpute" >					
				<option value="4" <?php selected($service_list, '4' ); ?>>4</option>
				<option value="8" <?php selected($service_list, '8' ); ?>>8</option>
				<option value="12" <?php selected($service_list, '12' ); ?>>12</option>
				<option value="16" <?php selected($service_list, '16' ); ?>>16</option>
				<option value="20" <?php selected($service_list, '20' ); ?>>20</option>
				<option value="22" <?php selected($service_list, '24' ); ?>>24</option>
			</select>
			
			<span class="explain"><?php _e('Select no of Services','quality'); ?></span>
		</div>
		<div class="section">		
			<h3><?php _e('Service Title','corpbiz'); ?></h3>
			<input class="webriti_inpute"  type="text" name="home_service_title" id="home_service_title" value="<?php echo $current_options['home_service_title']; ?>" >
			<span class="explain"><?php _e('Enter the Service Title.','corpbiz'); ?></span>
		</div>
		<div class="section">	
		<h3><?php _e('Service Description','corpbiz'); ?></h3>			
			<textarea rows="3" cols="8" id="home_service_description" name="home_service_description"><?php if($current_options['home_service_description']!='') { echo esc_attr($current_options['home_service_description']); } ?></textarea>
			<span class="explain"><?php _e('Enter the Service Description.','corpbiz'); ?></span>
		</div>		
		<div id="button_section">
			<input type="hidden" value="1" id="webriti_settings_save_14" name="webriti_settings_save_14" />
			<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('14');">
			<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('14')" >
		</div>
		<div class="webriti_spacer"></div>
	</form>
</div>