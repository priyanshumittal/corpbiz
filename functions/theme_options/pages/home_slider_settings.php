<div class="block ui-tabs-panel deactive" id="option-ui-id-2" >	
	<?php $current_options = wp_parse_args(  get_option( 'corpbiz_options', array() ), theme_data_setup() );
	if(isset($_POST['webriti_settings_save_2']))
	{	
		if($_POST['webriti_settings_save_2'] == 1) 
		{
			if ( empty($_POST) || !wp_verify_nonce($_POST['webriti_gernalsetting_nonce_customization'],'webriti_customization_nonce_gernalsetting') )
			{  printf (__('Sorry, your nonce did not verify.','corpbiz'));	exit; }
			else  
			{	
			
				// slider section enabled yes ya on
				if(isset($_POST['home_post_enabled']))
				{ echo $current_options['home_post_enabled']= sanitize_text_field($_POST['home_post_enabled']); } 
				else { echo $current_options['home_post_enabled']="off"; } 
				$current_options['animation']=sanitize_text_field($_POST['animation']);
				$current_options['animationSpeed']=sanitize_text_field($_POST['animationSpeed']);
				$current_options['slide_direction']=sanitize_text_field($_POST['slide_direction']);
				$current_options['slideshowSpeed']=sanitize_text_field($_POST['slideshowSpeed']);
			
			
				$current_options['slider_radio']=sanitize_text_field($_POST['slider_radio']);
				$current_options['slider_options']=sanitize_text_field($_POST['slider_options']);
				$current_options['slider_transition_delay']=sanitize_text_field($_POST['slider_transition_delay']);
				$current_options['featured_slider_post']=sanitize_text_field($_POST['featured_slider_post']);
				$total_slide = $_POST['total_slide'];
				$slider_list=array();
				for($i=1; $i<= $total_slide; $i++)
				{	
				$slider_image ='slider_image_'.$i;
				$slider_title ='slider_title_'.$i;
				$slider_title_one ='slider_description_'.$i;
				
				$slider_btn_text ='slider_btn_text_'.$i;
				$slider_btn_link ='slider_btn_link_'.$i;
				$slider_btn_link_target ='slider_btn_link_target_'.$i;
				
				$slider_title = $_POST[$slider_title];					
				$slider_title_one = $_POST[$slider_title_one];
				
				$slider_btn_text = $_POST[$slider_btn_text];
				$slider_btn_link = $_POST[$slider_btn_link];
				$slider_btn_link_target = $_POST[$slider_btn_link_target];
				$slider_image = $_POST[$slider_image];					
				$slider_list[$i]=array('slider_title'=> $slider_title,'slider_title_one'=>$slider_title_one,'slider_btn_text'=> $slider_btn_text,'slider_btn_link'=> $slider_btn_link,'slider_btn_link_target'=> $slider_btn_link_target, 'slider_image_url'=>$slider_image);
				
			}	
				$current_options['slider_list']= $slider_list;
				$current_options['total_slide']= $_POST['total_slide'];
				
				$all_cats=$_POST['slider_select_category'];
				if($all_cats)
				{
					$arr=' ';
					foreach($all_cats as $val)
					{
						$arr.=$val.',';
					}
					$current_options['slider_select_category']=$arr;
				}
				// slider section enabled yes ya on
				if(isset($_POST['home_banner_enabled']))
				{ echo $current_options['home_banner_enabled']= sanitize_text_field($_POST['home_banner_enabled']); } 
				else { echo $current_options['home_banner_enabled']="off"; } 
				
				update_option('corpbiz_options', $current_options);
			}
		}	
		 if($_POST['webriti_settings_save_2'] == 2) 
		{
			
			$current_options['home_banner_enabled']='on';
			$current_options['home_post_enabled']='on';
			$current_options['slider_btn_link_target']= 'on';
			$current_options['slider_radio']= 'demo';
			$current_options['slider_select_category']= ' Uncategorized ';
			$current_options['featured_slider_post']= '';

			$current_options['home_slider_enabled']="on";
			$current_options['animation']='slide';
			$current_options['animationSpeed']='1500';
			$current_options['slide_direction']='horizontal';
			$current_options['slideshowSpeed']='2500';
			update_option('corpbiz_options',$current_options);
		} 
	}  ?>
<script type="text/javascript">
	function addInput1() 
	{	
	  var slider =jQuery('#total_slide').val();
	  slider++;
	  jQuery('#webriti_slider').append('<div id="slider-slider-'+slider+'" class="section" ><h3>slider Title '+slider+' </h3><input class="webriti_inpute" type="text" value="" id="slider_title_'+slider+'" name="slider_title_'+slider+'" size="36"/><h3>slider description '+slider+' </h3><input class="webriti_inpute" type="text" value="" id="slider_description_'+slider+'" name="slider_description_'+slider+'" size="36"/><h3>slider Image '+slider+' </h3><input class="webriti_inpute" type="text" value="" id="slider_image_'+slider+'" name="slider_image_'+slider+'" size="36"/><input type="button" id="upload_image_button_'+slider+'" value="upload slider image" class="upload_image_button_'+slider+'" onClick="webriti_slider('+slider+')" /></div>');
	  
	  jQuery("#remove_button").show();
	  jQuery('#total_slide').val(slider);
	}

	function remove_field1()
	{
		var slider =jQuery('#total_slide').val();
		if(slider){
		jQuery("#slider-slider-"+slider).remove();
		slider=slider-1;
		jQuery('#total_slide').val(slider);
		}
	}

	function webriti_slider(slider_id)
	{
		// media upload js
		var uploadID = ''; /*setup the var*/
		//jQuery('.upload_image_button').click(function() {
		var upload_image_button="#upload_image_button_"+slider_id;
			uploadID = jQuery(upload_image_button).prev('input'); /*grab the specific input*/			
			formfield = jQuery('.upload').attr('name');
			tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
			
			window.send_to_editor = function(html)
			{
				imgurl = jQuery('img',html).attr('src');
				uploadID.val(imgurl); /*assign the value to the input*/
				tb_remove();
			};		
			return false;
		//});
	}
</script>
	<form method="post" id="webriti_theme_options_2">
		<div id="heading">
			<table style="width:100%;"><tr>
				<td><h2><?php _e('Home Feature Image Setting','corpbiz');?></h2></td>
				<td><div class="webriti_settings_loding" id="webriti_loding_2_image"></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_2_success" ><?php _e('Options data successfully Saved','corpbiz');?></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_2_reset" ><?php _e('Options data successfully reset','corpbiz');?></div>
				</td>
				<td style="text-align:right;">
					<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('2');">
					<input class="button button-primary button-large" type="button" value="Save Options" onclick="webriti_option_data_save('2')" >
				</td>
				</tr>
			</table>	
		</div>		
		<?php wp_nonce_field('webriti_customization_nonce_gernalsetting','webriti_gernalsetting_nonce_customization'); ?>
		<div class="section">
			<h3><?php _e('Enable Home Banner','corpbiz'); ?>  </h3>
			<input type="checkbox" <?php if($current_options['home_banner_enabled']=='on') echo "checked='checked'"; ?> id="home_banner_enabled" name="home_banner_enabled" >
			<span class="explain"><?php _e('Enable Home Banner on front page.','corpbiz'); ?></span>
		</div>
		<div class="section">
			<h3><?php _e('Select Slider Type','corpbiz'); ?>  </h3>
			<input type="radio" name="slider_radio" id="slider_radio_demo" value="demo" <?php if($current_options['slider_radio']=='demo'){echo 'checked';} ?>><?php _e('Demo slider','corpbiz'); ?>
			<input type="radio" name="slider_radio" id="slider_radio_post" value="post" <?php if($current_options['slider_radio']=='post'){echo 'checked';} ?>><?php _e('Post slider','corpbiz'); ?>
			<input type="radio" name="slider_radio"  id="slider_radio_category" value="category" <?php if($current_options['slider_radio']=='category'){echo 'checked';} ?>> <?php _e('Category slider','corpbiz'); ?>
			<input type="radio" name="slider_radio"  id="slider_radio_image" value="image" <?php if($current_options['slider_radio']=='image'){echo 'checked';} ?>> <?php _e('Image slider','corpbiz'); ?>
		</div>
		<div id="main_section" class="section" <?php if ($current_options['slider_radio']!=='demo'){echo 'style="display:none;"';}?>>
		<div class="section">
			<h3><?php _e('Animation','corpbiz'); ?></h3>
			<?php $animation = $current_options['animation']; ?>		
			<select name="animation" class="webriti_inpute" >					
				<option value="fade"  <?php echo selected($animation, 'fade' ); ?>><?php _e('fade','corpbiz');?></option>
			</select>
			<span class="explain"><?php _e('Select the Animation Type.','corpbiz'); ?></span>
		</div>
		<div class="section">
			<h3><?php _e('Animation speed','corpbiz') ?></h3>
			<?php $animationSpeed = $current_options['animationSpeed']; ?>		
				<select name="animationSpeed" class="webriti_inpute" >					
					<option value="500" <?php selected($animationSpeed, '500' ); ?>>0.5</option>
					<option value="1000" <?php selected($animationSpeed, '1000' ); ?>>1.0</option>
					<option value="1500" <?php selected($animationSpeed, '1500' ); ?>>1.5</option>
					<option value="2000" <?php selected($animationSpeed, '2000' ); ?>>2.0</option>
					<option value="2500" <?php selected($animationSpeed, '2500' ); ?>>2.5</option>
					<option value="3000" <?php selected($animationSpeed, '3000' ); ?>>3.0</option>
					<option value="3500" <?php selected($animationSpeed, '3500' ); ?>>3.5</option>
					<option value="4000" <?php selected($animationSpeed, '4000' ); ?>>4.0</option>
					<option value="4500" <?php selected($animationSpeed, '4500' ); ?>>4.5</option>
					<option value="5000" <?php selected($animationSpeed, '5000' ); ?>>5.0</option>
					<option value="5500" <?php selected($animationSpeed, '5500' ); ?>>5.5</option>
				</select>
				<span class="explain"><?php _e('Select Slide Animation speed.','corpbiz'); ?></span>	
		</div>
		<div class="section">
			<h3><?php _e('Slideshow speed','corpbiz'); ?></h3>
			<?php $slideshowSpeed = $current_options['slideshowSpeed']; ?>		
			<select name="slideshowSpeed" class="webriti_inpute">					
				<option value="500" <?php selected($slideshowSpeed, '500' ); ?>>0.5</option>
				<option value="1000" <?php selected($slideshowSpeed, '1000' ); ?>>1.0</option>
				<option value="1500" <?php selected($slideshowSpeed, '1500' ); ?>>1.5</option>
				<option value="2000" <?php selected($slideshowSpeed, '2000' ); ?>>2.0</option>
				<option value="2500" <?php selected($slideshowSpeed, '2500' ); ?>>2.5</option>
				<option value="3000" <?php selected($slideshowSpeed, '3000' ); ?>>3.0</option>
				<option value="3500" <?php selected($slideshowSpeed, '3500' ); ?>>3.5</option>
				<option value="4000" <?php selected($slideshowSpeed, '4000' ); ?>>4.0</option>
				<option value="4500" <?php selected($slideshowSpeed, '4500' ); ?>>4.5</option>
				<option value="5000" <?php selected($slideshowSpeed, '5000' ); ?>>5.0</option>
				<option value="5500" <?php selected($slideshowSpeed, '5500' ); ?>>5.5</option>
			</select>
			<span class="explain"><?php _e('Select the Slide Show Speed.','corpbiz'); ?></span>
		</div>
		</div>
		
		<div id="post_slider" <?php if($current_options['slider_radio']!='post'){echo 'style="display:none;"';}?>>
			<input type="checkbox" <?php if($current_options['home_post_enabled']=='on') echo "checked='checked'"; ?> id="home_post_enabled" name="home_post_enabled" value="on">
			<span class="explain"><?php _e('Enable Home Post on front page.','corpbiz'); ?></span>
			
			<h3><?php _e('Featured post slider section','corpbiz'); ?> </h3>
			<div id="all_slider_content">
				<div class="repeat-content-wrap">
					<div class="row"> 
						<div class="col col-1"> <?php _e('Featured Post Slider','corpbiz'); ?>
						</div>
						<div class="col col-2">
							<input type="text" name="featured_slider_post" value="<?php echo $current_options['featured_slider_post'];?>">
							<a href="http://localhost/wordpress_elegent/wp-admin/post.php?post=&amp;action=edit" class="button" title="Click Here To Edit" target="_blank"><?php _e('Click Here To Edit','corpbiz'); ?></a>
							<p><span> <h5><?php _e("You can use multiple ID's seprated by Commma[ , ]",'corpbiz');?></span></h5></p>						</div>
					</div>
				</div>
			</div>
		</div>	
		<div id="category_slider" <?php if($current_options['slider_radio']!='category'){echo 'style="display:none;"';}?>>
			<h3><?php _e('Featured Category slider section','corpbiz'); ?> </h3>
			
			<div>
				<select class="slider_select_cat" name="slider_select_category[]" multiple >
				
				<?php
				$args = array(
				  'orderby' => 'name',
				  'parent' => 0
				  );
				$categories = get_categories( $args );
				foreach ( $categories as $category ) {
				?>
					<option <?php if(!strpos($current_options['slider_select_category'],$category->name)===false){echo 'selected';}?> > <?php echo $category->name;?> </option><?php
				}
				?>
				</select>
			</div>
		</div>
		
		<div id="demo_slider" <?php if ($current_options['slider_radio']!='demo'){echo 'style="display:none;"';}?>>
		</div>	
		<div class="section">
		</div>
		<div id="webriti_slider" <?php if ($current_options['slider_radio']!='image'){echo 'style="display:none;"';}?>>
		<?php if($current_options['slider_list'])
		{
			$i=1;
			foreach($current_options['slider_list'] as $slider_list)
			{	?>
				<div class="section" id="slider-slider-<?php echo $i ?>"> 
					<h3><?php _e('Slider Title','corpbiz'); ?> <?php echo $i ?></h3>
					<input type="text" value="<?php echo $slider_list['slider_title']; ?>" id="slider_title_<?php echo $i ?>" name="slider_title_<?php echo $i ?>" class="webriti_inpute">
					<h3><?php _e('Slider Description','corpbiz'); ?><?php echo $i ?></h3>
					<input type="text" value="<?php echo $slider_list['slider_title_one']; ?>" id="slider_description_<?php echo $i ?>" name="slider_description_<?php echo $i ?>" class="webriti_inpute">
					<input type="text" value="<?php echo $slider_list['slider_image_url']; ?>" id="slider_image_<?php echo $i ?>" name="slider_image_<?php echo $i ?>" class="webriti_inpute">
					<input type="button" id="upload_button" value="slider Image" class="upload_image_button"  />			<BR>
					<img src="<?php echo $slider_list['slider_image_url']; ?>" style="height:150px; width:250px;">
				</div>	
			<?php	$i=$i+1;
			}	
		} ?>
		<div class="section" style="margin-bottom:30px;">
			<a onclick="addInput1()" href="#" class="btn btn-primary" name="add" id="more_faq" ><?php _e('Add Slide','corpbiz');?></a>
			<a onclick="remove_field1()" href="#" class="btn btn-inverse"  id="remove_button" style="display:<?php if(!$current_options['total_slide']) { ?>none<?php } ?>;"><?php _e('Remove Last Slide','corpbiz'); ?></a>		
		</div>
		<input type="hidden" class="webriti_inpute" type="text" id="total_slide" name="total_slide" value="<?php echo $current_options['total_slide']; ?> " />
		
		</div>
		
		
		<div id="button_section">
			<input type="hidden" value="1" id="webriti_settings_save_2" name="webriti_settings_save_2" />
			<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('2');">
			<input class="button button-primary button-large" type="button" value="Save Options" onclick="webriti_option_data_save('2')" >
		</div>
	</form>
</div>
<script>                         
	
	
  jQuery('input[name=slider_radio]').on('click',function(){
  if(this.value=='category')
  {
	jQuery("#category_slider").attr('style','display:block');
	jQuery("#post_slider").attr('style','display:none');
	jQuery("#demo_slider").attr('style','display:none');
	jQuery("#main_section").attr('style','display:block');
	jQuery("#webriti_slider").attr('style','display:none');
	
	webriti_slider
  }
  else if(this.value=='post')
  {
	jQuery("#category_slider").attr('style','display:none');
	jQuery("#post_slider").attr('style','display:block');
	jQuery("#demo_slider").attr('style','display:none');
	jQuery("#main_section").attr('style','display:block');
	jQuery("#webriti_slider").attr('style','display:none');
  }
  else if(this.value=='image')
  {
  jQuery("#category_slider").attr('style','display:none');
  jQuery("#post_slider").attr('style','display:none');
  jQuery("#demo_slider").attr('style','display:none');
  jQuery("#main_section").attr('style','display:block');
  jQuery("#webriti_slider").attr('style','display:block');
  }
  else
  {
  jQuery("#category_slider").attr('style','display:none');
  jQuery("#post_slider").attr('style','display:none');
  jQuery("#demo_slider").attr('style','display:block');
  jQuery("#main_section").attr('style','display:block');
  jQuery("#webriti_slider").attr('style','display:none');
  }
  });
  </script>
  </script>