<div class="block ui-tabs-panel deactive" id="option-ui-id-12" >	
	<?php $current_options = wp_parse_args(  get_option( 'corpbiz_options', array() ), theme_data_setup() );
	if(isset($_POST['webriti_settings_save_12']))
	{	
		if($_POST['webriti_settings_save_12'] == 1) 
		{
			if ( empty($_POST) || !wp_verify_nonce($_POST['webriti_gernalsetting_nonce_customization'],'webriti_customization_nonce_gernalsetting') )
			{  print 'Sorry, your nonce did not verify.';	exit; }
			else  
			{		
				if(isset($_POST['enable_custom_typography']))
				{ 	$current_options['enable_custom_typography']= sanitize_text_field($_POST['enable_custom_typography']); } 
				else
				{  $current_options['enable_custom_typography']="off"; } 
				
				// general typography
				$current_options['general_typography_fontsize']=sanitize_text_field($_POST['general_typography_fontsize']);	
				$current_options['general_typography_fontfamily']=sanitize_text_field($_POST['general_typography_fontfamily']);	
				$current_options['general_typography_fontstyle']=sanitize_text_field($_POST['general_typography_fontstyle']);
				
				// menu title
				$current_options['menu_title_fontsize']=sanitize_text_field($_POST['menu_title_fontsize']);	
				$current_options['menu_title_fontfamily']=sanitize_text_field($_POST['menu_title_fontfamily']);	
				$current_options['menu_title_fontstyle']=sanitize_text_field($_POST['menu_title_fontstyle']);
				
				// post title
				$current_options['post_title_fontsize']=sanitize_text_field($_POST['post_title_fontsize']);	
				$current_options['post_title_fontfamily']=sanitize_text_field($_POST['post_title_fontfamily']);	
				$current_options['post_title_fontstyle']=sanitize_text_field($_POST['post_title_fontstyle']);
								
				// Service  title
				$current_options['service_title_fontsize']=sanitize_text_field($_POST['service_title_fontsize']);	
				$current_options['service_title_fontfamily']=sanitize_text_field($_POST['service_title_fontfamily']);	
				$current_options['service_title_fontstyle']=sanitize_text_field($_POST['service_title_fontstyle']);
				
				// Potfolio  title Widget Heading Title
				$current_options['portfolio_title_fontsize']=sanitize_text_field($_POST['portfolio_title_fontsize']);	
				$current_options['portfolio_title_fontfamily']=sanitize_text_field($_POST['portfolio_title_fontfamily']);	
				$current_options['portfolio_title_fontstyle']=sanitize_text_field($_POST['portfolio_title_fontstyle']);
				
				// Widget Heading Title
				$current_options['widget_title_fontsize']=sanitize_text_field($_POST['widget_title_fontsize']);	
				$current_options['widget_title_fontfamily']=sanitize_text_field($_POST['widget_title_fontfamily']);	
				$current_options['widget_title_fontstyle']=sanitize_text_field($_POST['widget_title_fontstyle']);
				
				// Call out area Title   
				$current_options['calloutarea_title_fontsize']=sanitize_text_field($_POST['calloutarea_title_fontsize']);	
				$current_options['calloutarea_title_fontfamily']=sanitize_text_field($_POST['calloutarea_title_fontfamily']);	
				$current_options['calloutarea_title_fontstyle']=sanitize_text_field($_POST['calloutarea_title_fontstyle']);
				
				// Call out area title     
				$current_options['calloutarea_description_fontsize']=sanitize_text_field($_POST['calloutarea_description_fontsize']);	
				$current_options['calloutarea_description_fontfamily']=sanitize_text_field($_POST['calloutarea_description_fontfamily']);	
				$current_options['calloutarea_description_fontstyle']=sanitize_text_field($_POST['calloutarea_description_fontstyle']);
								
				// Call out area purches button      
							
				update_option('corpbiz_options', stripslashes_deep($current_options));
			
		}	
		}	
		if($_POST['webriti_settings_save_12'] == 2) 
		{
				
				// typography enabled yes ya on
				$current_options['enable_custom_typography']="off";
				// general typography
				$current_options['general_typography_fontsize']='16';
				$current_options['general_typography_fontfamily']='RobotoLight';
				$current_options['general_typography_fontstyle']="";
				
				// menu title
				$current_options['menu_title_fontsize']='15';
				$current_options['menu_title_fontfamily']='RobotoMedium';
				$current_options['menu_title_fontstyle']="";
				
				// post title
				$current_options['post_title_fontsize']='32';
				$current_options['post_title_fontfamily']='RobotoLight';
				$current_options['post_title_fontstyle']= "";				
				
				// Service  title
				$current_options['service_title_fontsize']='20';
				$current_options['service_title_fontfamily']='RobotoMedium';
				$current_options['service_title_fontstyle']="";
				
				// Potfolio  title Widget Heading Title
				$current_options['portfolio_title_fontsize']='20';
				$current_options['portfolio_title_fontfamily']='RobotoMedium';
				$current_options['portfolio_title_fontstyle']="";
				
				// Widget Heading Title
				$current_options['widget_title_fontsize']='28';
				$current_options['widget_title_fontfamily']='RobotoLight';
				$current_options['widget_title_fontstyle']="";
				
				// Call out area Title   
				$current_options['calloutarea_title_fontsize']='36';
				$current_options['calloutarea_title_fontfamily']='RobotoLight';
				$current_options['calloutarea_title_fontstyle']="";
				
				// Call out area descritpion      
				$current_options['calloutarea_description_fontsize']='15';
				$current_options['calloutarea_description_fontfamily']='RobotoRegular';
				$current_options['calloutarea_description_fontstyle']="";
				
			update_option('corpbiz_options',$current_options);
		}
	}  ?>
	<form method="post" id="webriti_theme_options_12">
		<div id="heading">
			<table style="width:100%;"><tr>
				<td><h2><?php _e('Typography','corpbiz');?></h2></td>
				<td><div class="webriti_settings_loding" id="webriti_loding_12_image"></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_12_success" ><?php _e('Options data successfully Saved','corpbiz');?></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_12_reset" ><?php _e('Options data successfully reset','corpbiz');?></div>
				</td>
				<td style="text-align:right;">
					<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('12');">
					<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('12')" >
				</td>
				</tr>
			</table>	
		</div>		
		<?php wp_nonce_field('webriti_customization_nonce_gernalsetting','webriti_gernalsetting_nonce_customization'); ?>
		<div class="section">
			<h3><?php _e('Enable Custom Typography','corpbiz');?></h3>
			<input type="checkbox" <?php if($current_options['enable_custom_typography']=='on') echo "checked='checked'"; ?> id="enable_custom_typography" name="enable_custom_typography" > <span class="explain"><?php _e('Enable the use of custom typography for your site.','corpbiz'); ?></span>
		</div>	
		<div class="section" id="General_Typography">
			<h3><?php _e('General Typography','corpbiz');?></h3>
			<?php $general_typography_fontsize = $current_options['general_typography_fontsize']; ?>
			<p><select name="general_typography_fontsize" id="general_typography_fontsize" class="select" >
					<?php for ($i = 9; $i <= 100; $i++) { ?><option value="<?php echo $i; ?>" <?php if ( $general_typography_fontsize == $i ) echo selected($general_typography_fontsize, $i ); ?> name=""><?php echo $i; ?></option><?php } ?>
				</select>
				<select  id="main_navigation"  class="select">
					<option value="px"><?php _e('px','corpbiz');?></option>
				</select>
			</p>
			<p><?php $general_typography_fontfamily = $current_options['general_typography_fontfamily']; ?>				
				<select id="" name="general_typography_fontfamily" class="select">
					<option value="RobotoRegular" <?php selected($general_typography_fontfamily, 'RobotoRegular' ); ?>>RobotoRegular</option>
					<option value="RobotoLight" <?php selected($general_typography_fontfamily, 'RobotoLight' ); ?>>RobotoLight</option>
					<option value="RobotoBold" <?php selected($general_typography_fontfamily, 'RobotoBold' ); ?>>RobotoBold</option>
					<option value="RobotoBlack" <?php selected($general_typography_fontfamily, 'RobotoBlack' ); ?>>RobotoBlack</option>
					<option value="RobotoMedium" <?php selected($general_typography_fontfamily, 'RobotoMedium' ); ?>>RobotoMedium</option>
					<option value="RobotoThin" <?php selected($general_typography_fontfamily, 'RobotoThin' ); ?>>RobotoThin</option>
				</select>	
					<?php $general_typography_fontstyle = $current_options['general_typography_fontstyle']; ?>
				<select id="general_typography_fontstyle" name="general_typography_fontstyle" class="select">
					<option value="normal" <?php selected($general_typography_fontstyle, 'normal' ); ?>><?php _e('Normal','corpbiz'); ?></option>
					<option value="italic" <?php selected($general_typography_fontstyle, 'italic' ); ?>><?php _e('Italic','corpbiz'); ?></option>
				</select>
			</p>			
		</div>	
		<div class="section" id="menus_title">
			<h3><?php _e('Menu','corpbiz');?></h3>
			<?php $menu_title_fontsize = $current_options['menu_title_fontsize']; ?>
			<p>	<select name="menu_title_fontsize" id="menu_title_fontsize" class="select" >
					<?php for ($i = 9; $i <= 100; $i++) { ?><option value="<?php echo $i; ?>" <?php if ( $menu_title_fontsize == $i ) echo selected($menu_title_fontsize, $i ); ?> name=""><?php echo $i; ?></option><?php } ?>
				</select>
				<select  id="main_navigation"  class="select">
					<option value="px"><?php _e('px','corpbiz');?></option>
				</select>
			</p>
			<p><?php $menu_title_fontfamily = $current_options['menu_title_fontfamily']; ?>
				<select id="" name="menu_title_fontfamily" class="select">
					<option value="RobotoRegular" <?php selected($menu_title_fontfamily, 'RobotoRegular' ); ?>>RobotoRegular</option>
					<option value="RobotoLight"  <?php selected($menu_title_fontfamily, 'RobotoLight' ); ?>>RobotoLight</option>
					<option value="RobotoBold" <?php selected($menu_title_fontfamily, 'RobotoBold' ); ?>>RobotoBold</option>
					<option value="RobotoBlack" <?php selected($menu_title_fontfamily, 'RobotoBlack' ); ?>>RobotoBlack</option>
					<option value="RobotoMedium" <?php selected($menu_title_fontfamily, 'RobotoMedium' ); ?>>RobotoMedium</option>
					<option value="RobotoThin" <?php selected($menu_title_fontfamily, 'RobotoThin' ); ?>>RobotoThin</option>
				</select>
				<?php $menu_title_fontstyle = $current_options['menu_title_fontstyle']; ?>
				<select id="menu_title_fontstyle" name="menu_title_fontstyle" class="select">
					<option value="normal" <?php selected($menu_title_fontstyle); ?>><?php _e('Normal','corpbiz'); ?></option>
					<option value="italic" <?php selected($menu_title_fontstyle); ?>><?php _e('Italic','corpbiz'); ?></option>
				</select>
			</p>
		</div>
		<div class="section" id="Post_title">
			<h3><?php _e('Post And Page Title','corpbiz');?></h3>
			<?php $post_title_fontsize = $current_options['post_title_fontsize']; ?>
			<p>	<select name="post_title_fontsize" id="post_title_fontsize" class="select" >
					<?php for ($i = 9; $i <= 100; $i++) { ?><option value="<?php echo $i; ?>" <?php if ( $post_title_fontsize == $i ) echo selected($post_title_fontsize, $i ); ?> name=""><?php echo $i; ?></option><?php } ?>
				</select>
				<select  id="main_navigation"  class="select">
					<option value="px"><?php _e('px','corpbiz');?></option>
				</select>
			</p>
			<p><?php $post_title_fontfamily = $current_options['post_title_fontfamily']; ?>	
				<select id="" name="post_title_fontfamily" class="select">
					<option value="RobotoRegular" <?php selected($post_title_fontfamily, 'RobotoRegular' ); ?>>RobotoRegular</option>
					<option value="RobotoLight"  <?php selected($post_title_fontfamily, 'RobotoLight' ); ?>>RobotoLight</option>
					<option value="RobotoBold" <?php selected($post_title_fontfamily, 'RobotoBold' ); ?>>RobotoBold</option>
					<option value="RobotoBlack" <?php selected($post_title_fontfamily, 'RobotoBlack' ); ?>>RobotoBlack</option>
					<option value="RobotoMedium" <?php selected($post_title_fontfamily, 'RobotoMedium' ); ?>>RobotoMedium</option>
					<option value="RobotoThin" <?php selected($post_title_fontfamily, 'RobotoThin' ); ?>>RobotoThin</option>
				</select>
				<?php $post_title_fontstyle = $current_options['post_title_fontstyle']; ?>
				<select id="post_title_fontstyle" name="post_title_fontstyle" class="select">
					<option value="normal" <?php selected($post_title_fontstyle); ?>><?php _e('Normal','corpbiz'); ?></option>
					<option value="italic" <?php selected($post_title_fontstyle); ?>><?php _e('Italic','corpbiz'); ?></option>
				</select>
			</p>
		</div>		
		<div class="section" id="service_title">
			<h3><?php _e('Service Title','corpbiz');?></h3>
			<?php $service_title_fontsize = $current_options['service_title_fontsize']; ?>
			<p>	<select name="service_title_fontsize" id="service_title_fontsize" class="select" >
					<?php for ($i = 9; $i <= 100; $i++) { ?><option value="<?php echo $i; ?>" <?php if ( $service_title_fontsize == $i ) echo selected($service_title_fontsize, $i ); ?> name=""><?php echo $i; ?></option><?php } ?>
				</select>
				<select  id="main_navigation"  class="select">
					<option value="px"><?php _e('px','corpbiz');?></option>
				</select>
			</p>
			<p><?php $service_title_fontfamily = $current_options['service_title_fontfamily']; ?>	
				<select id="" name="service_title_fontfamily" class="select">
					<option value="RobotoRegular" <?php selected($service_title_fontfamily, 'RobotoRegular' ); ?>>RobotoRegular</option>
					<option value="RobotoLight"  <?php selected($service_title_fontfamily, 'RobotoLight' ); ?>>RobotoLight</option>
					<option value="RobotoBold" <?php selected($service_title_fontfamily, 'RobotoBold' ); ?>>RobotoBold</option>
					<option value="RobotoBlack" <?php selected($service_title_fontfamily, 'RobotoBlack' ); ?>>RobotoBlack</option>
					<option value="RobotoMedium" <?php selected($service_title_fontfamily, 'RobotoMedium' ); ?>>RobotoMedium</option>
					<option value="RobotoThin" <?php selected($service_title_fontfamily, 'RobotoThin' ); ?>>RobotoThin</option>
				
				</select>
				<?php $service_title_fontstyle = $current_options['service_title_fontstyle']; ?>
				<select id="service_title_fontstyle" name="service_title_fontstyle" class="select">
					<option value="normal" <?php selected($service_title_fontstyle); ?>><?php _e('Normal','corpbiz'); ?></option>
					<option value="italic" <?php selected($service_title_fontstyle); ?>><?php _e('Italic','corpbiz'); ?></option>
				</select>
			</p>
		</div>
		<div class="section" id="portfolio_title">
			<h3><?php _e('Portfolio Title','corpbiz');?></h3>
			<?php  $portfolio_title_fontsize = $current_options['portfolio_title_fontsize']; ?>
			<p>	<select name="portfolio_title_fontsize" id="portfolio_title_fontsize" class="select" >
					<?php for ($i = 9; $i <= 100; $i++) { ?><option value="<?php echo $i; ?>" <?php if ( $portfolio_title_fontsize == $i ) echo selected($portfolio_title_fontsize, $i ); ?> ><?php echo $i; ?></option><?php } ?>
				</select>
				<select  id="main_navigation"  class="select">
					<option value="px"><?php _e('px','corpbiz');?></option>
				</select>
			</p>
			<p><?php $portfolio_title_fontfamily = $current_options['portfolio_title_fontfamily']; ?>	
				<select id="" name="portfolio_title_fontfamily" class="select">
					<option value="RobotoRegular" <?php selected($portfolio_title_fontfamily, 'RobotoRegular' ); ?>>RobotoRegular</option>
					<option value="RobotoLight"  <?php selected($portfolio_title_fontfamily, 'RobotoLight' ); ?>>RobotoLight</option>
					<option value="RobotoBold" <?php selected($portfolio_title_fontfamily, 'RobotoBold' ); ?>>RobotoBold</option>
					<option value="RobotoBlack" <?php selected($portfolio_title_fontfamily, 'RobotoBlack' ); ?>>RobotoBlack</option>
					<option value="RobotoMedium" <?php selected($portfolio_title_fontfamily, 'RobotoMedium' ); ?>>RobotoMedium</option>
					<option value="RobotoThin" <?php selected($portfolio_title_fontfamily, 'RobotoThin' ); ?>>RobotoThin</option>
				
				</select>
				<?php $portfolio_title_fontstyle = $current_options['portfolio_title_fontstyle']; ?>	
				<select id="portfolio_title_fontstyle" name="portfolio_title_fontstyle" class="select">
					<option value="normal" <?php selected($portfolio_title_fontstyle); ?>><?php _e('Normal','corpbiz'); ?></option>
					<option value="italic" <?php selected($portfolio_title_fontstyle); ?>><?php _e('Italic','corpbiz'); ?></option>
				</select>
			</p>
		</div>
		<div class="section" id="widget_title">
			<h3><?php _e('Widget Heading Title','corpbiz');?></h3>
			<?php  $widget_title_fontsize = $current_options['widget_title_fontsize']; ?>
			<p>	<select name="widget_title_fontsize" id="widget_title_fontsize" class="select" >
					<?php for ($i = 9; $i <= 100; $i++) { ?><option value="<?php echo $i; ?>" <?php if ( $widget_title_fontsize == $i ) echo selected($widget_title_fontsize, $i ); ?> ><?php echo $i; ?></option><?php } ?>
				</select>
				<select  id="main_navigation"  class="select">
					<option value="px"><?php _e('px','corpbiz');?></option>
				</select>
			</p>
			<p><?php $widget_title_fontfamily = $current_options['widget_title_fontfamily']; ?>
				<select id="" name="widget_title_fontfamily" class="select">
					<option value="RobotoRegular" <?php selected($widget_title_fontfamily, 'RobotoRegular' ); ?>>RobotoRegular</option>
					<option value="RobotoLight"  <?php selected($widget_title_fontfamily, 'RobotoLight' ); ?>>RobotoLight</option>
					<option value="RobotoBold" <?php selected($widget_title_fontfamily, 'RobotoBold' ); ?>>RobotoBold</option>
					<option value="RobotoBlack" <?php selected($widget_title_fontfamily, 'RobotoBlack' ); ?>>RobotoBlack</option>
					<option value="RobotoMedium" <?php selected($widget_title_fontfamily, 'RobotoMedium' ); ?>>RobotoMedium</option>
					<option value="RobotoThin" <?php selected($widget_title_fontfamily, 'RobotoThin' ); ?>>RobotoThin</option>
				
				</select>
				<?php $widget_title_fontstyle = $current_options['widget_title_fontstyle']; ?>
				<select id="widget_title_fontstyle" name="widget_title_fontstyle" class="select">
					<option value="normal" <?php selected($widget_title_fontstyle); ?>><?php _e('Normal','corpbiz'); ?></option>
					<option value="italic" <?php selected($widget_title_fontstyle); ?>><?php _e('Italic','corpbiz'); ?></option>
				</select>
			</p>
		</div>
		<div class="section" id="calloutarea_title">
			<h3><?php _e('Call out area Title','corpbiz');?></h3>			
			<?php $calloutarea_title_fontsize = $current_options['calloutarea_title_fontsize']; ?>
			<p>	<select name="calloutarea_title_fontsize" id="calloutarea_title_fontsize" class="select" >
					<?php for ($i = 9; $i <= 100; $i++) { ?><option value="<?php echo $i; ?>" <?php if ( $calloutarea_title_fontsize == $i ) echo selected($calloutarea_title_fontsize, $i ); ?> name=""><?php echo $i; ?></option><?php } ?>
				</select>
				<select  id="main_navigation"  class="select">
					<option value="px"><?php _e('px','corpbiz');?></option>
				</select>
			</p>
			<p><?php $calloutarea_title_fontfamily = $current_options['calloutarea_title_fontfamily']; ?>
				<select id="" name="calloutarea_title_fontfamily" class="select">
					<option value="RobotoRegular" <?php selected($calloutarea_title_fontfamily, 'RobotoRegular' ); ?>>RobotoRegular</option>
					<option value="RobotoLight"  <?php selected($calloutarea_title_fontfamily, 'RobotoLight' ); ?>>RobotoLight</option>
					<option value="RobotoBold" <?php selected($calloutarea_title_fontfamily, 'RobotoBold' ); ?>>RobotoBold</option>
					<option value="RobotoBlack" <?php selected($calloutarea_title_fontfamily, 'RobotoBlack' ); ?>>RobotoBlack</option>
					<option value="RobotoMedium" <?php selected($calloutarea_title_fontfamily, 'RobotoMedium' ); ?>>RobotoMedium</option>
					<option value="RobotoThin" <?php selected($calloutarea_title_fontfamily, 'RobotoThin' ); ?>>RobotoThin</option>
				
				</select>
				<?php $calloutarea_title_fontstyle = $current_options['calloutarea_title_fontstyle']; ?>
				<select id="calloutarea_title_fontstyle" name="calloutarea_title_fontstyle" class="select">
					<option value="normal" <?php selected($calloutarea_title_fontstyle); ?>><?php _e('Normal','corpbiz'); ?></option>
					<option value="italic" <?php selected($calloutarea_title_fontstyle); ?>><?php _e('Italic','corpbiz'); ?></option>
				</select>
			</p>
			</div>
			<div class="section" id="calloutarea_title">
			<h3><?php _e('Call Out Area Description','corpbiz');?></h3>		
			<?php  $calloutarea_description_fontsize = $current_options['calloutarea_description_fontsize']; ?>
			<p>	<select name="calloutarea_description_fontsize" id="calloutarea_description_fontsize" class="select" >
					<?php for ($i = 9; $i <= 100; $i++) { ?><option value="<?php echo $i; ?>" <?php if ( $calloutarea_description_fontsize == $i ) echo selected($calloutarea_description_fontsize, $i ); ?> ><?php echo $i; ?></option><?php } ?>
				</select>
				<select  id="main_navigation"  class="select">
					<option value="px"><?php _e('px','corpbiz');?></option>
				</select>
			</p>
			<p><?php $calloutarea_description_fontfamily = $current_options['calloutarea_description_fontfamily']; ?>	
				<select id="" name="calloutarea_description_fontfamily" class="select">
					<option value="RobotoRegular" <?php selected($calloutarea_description_fontfamily, 'RobotoRegular' ); ?>>RobotoRegular</option>
					<option value="RobotoLight"  <?php selected($calloutarea_description_fontfamily, 'RobotoLight' ); ?>>RobotoLight</option>
					<option value="RobotoBold" <?php selected($calloutarea_description_fontfamily, 'RobotoBold' ); ?>>RobotoBold</option>
					<option value="RobotoBlack" <?php selected($calloutarea_description_fontfamily, 'RobotoBlack' ); ?>>RobotoBlack</option>
					<option value="RobotoMedium" <?php selected($calloutarea_description_fontfamily, 'RobotoMedium' ); ?>>RobotoMedium</option>
					<option value="RobotoThin" <?php selected($calloutarea_description_fontfamily, 'RobotoThin' ); ?>>RobotoThin</option>
				
				</select>
				<?php $calloutarea_description_fontstyle = $current_options['calloutarea_description_fontstyle']; ?>
				<select id="calloutarea_description_fontstyle" name="calloutarea_description_fontstyle" class="select">
					<option value="normal" <?php selected($calloutarea_description_fontstyle); ?>><?php _e('Normal','corpbiz'); ?></option>
					<option value="italic" <?php selected($calloutarea_description_fontstyle); ?>><?php _e('Italic','corpbiz'); ?></option>
				</select>
			</p>
			</div>		
		<div id="button_section">
			<input type="hidden" value="1" id="webriti_settings_save_12" name="webriti_settings_save_12" />
			<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('12');">
			<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('12')" >
		</div>
	</form>
</div>