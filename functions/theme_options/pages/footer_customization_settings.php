<div class="block ui-tabs-panel deactive" id="option-ui-id-11" >	
	<?php $current_options = wp_parse_args(  get_option( 'corpbiz_options', array() ), theme_data_setup() );
	//print_r($current_options);
	if(isset($_POST['webriti_settings_save_11']))
	{	
		if($_POST['webriti_settings_save_11'] == 1) 
		{
			if ( empty($_POST) || !wp_verify_nonce($_POST['webriti_gernalsetting_nonce_customization'],'webriti_customization_nonce_gernalsetting') )
			{  print 'Sorry, your nonce did not verify.';	exit; }
			else  
			{		
				$current_options['footer_copyright_text']=$_POST['footer_copyright_text'];
				update_option('corpbiz_options', stripslashes_deep($current_options));
			}
		}	
		if($_POST['webriti_settings_save_11'] == 2) 
		{
			$current_options['footer_copyright_text']='<p>@ Copyright 2014  Corpbiz Design And Developed by  <a href="http://www.webriti.com/" target="_blank">Wordpress Theme</a></p>';
			update_option('corpbiz_options', $current_options);
		}
	}  ?>
	<form method="post" id="webriti_theme_options_11">
		<div id="heading">
			<table style="width:100%;"><tr>
				<td><h2><?php _e('Footer Customizations','corpbiz');?></h2></td>
				<td><div class="webriti_settings_loding" id="webriti_loding_11_image"></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_11_success" ><?php _e('Options data successfully Saved','corpbiz');?></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_11_reset" ><?php _e('Options data successfully reset','corpbiz');?></div>
				</td>
				<td style="text-align:right;">
					<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('11');">
					<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('11')" >
				</td>
				</tr>
			</table>	
		</div>		
		<?php wp_nonce_field('webriti_customization_nonce_gernalsetting','webriti_gernalsetting_nonce_customization'); ?>
		<div class="section">		
			<h3><?php _e('Footer Customization text','corpbiz'); ?></h3>
			<textarea rows="8" cols="8" id="footer_copyright_text" name="footer_copyright_text"><?php if(isset($current_options['footer_copyright_text'])) 
			{ esc_attr_e($current_options['footer_copyright_text']); } ?></textarea>
			<span class="explain"><?php  _e('Enter your Footer Customization text','corpbiz');?></span>
		</div>		
				
		<div id="button_section">
			<input type="hidden" value="1" id="webriti_settings_save_11" name="webriti_settings_save_11" />
			<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('11');">
			<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('11')" >
		</div>
	</form>
</div>