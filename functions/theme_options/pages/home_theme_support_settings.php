<div class="block ui-tabs-panel deactive" id="option-ui-id-15" >	
	<?php $current_options = wp_parse_args(  get_option( 'corpbiz_options', array() ), theme_data_setup() );
	if(isset($_POST['webriti_settings_save_15']))
	{	
		if($_POST['webriti_settings_save_15'] == 1) 
		{
			if ( empty($_POST) || !wp_verify_nonce($_POST['webriti_gernalsetting_nonce_customization'],'webriti_customization_nonce_gernalsetting') )
			{  print 'Sorry, your nonce did not verify.';	exit; }
			else  
			{		
				$current_options['home_theme_support_title'] = sanitize_text_field($_POST['home_theme_support_title']);
				$current_options['home_theme_support_description'] = sanitize_text_field($_POST['home_theme_support_description']);
				
				$current_options['home_support_icon_one'] = sanitize_text_field($_POST['home_support_icon_one']);
				$current_options['home_support_title_one'] = sanitize_text_field($_POST['home_support_title_one']);
				$current_options['home_support_desciption_one'] = sanitize_text_field($_POST['home_support_desciption_one']);
				$current_options['home_support_learn_more_text_one'] = sanitize_text_field($_POST['home_support_learn_more_text_one']);
				$current_options['home_support_learn_more_link_one'] = sanitize_text_field($_POST['home_support_learn_more_link_one']);
				
				$current_options['home_support_icon_two'] = sanitize_text_field($_POST['home_support_icon_two']);
				$current_options['home_support_title_two'] = sanitize_text_field($_POST['home_support_title_two']);
				$current_options['home_support_desciption_two'] = sanitize_text_field($_POST['home_support_desciption_two']);
				$current_options['home_support_learn_more_text_two'] = sanitize_text_field($_POST['home_support_learn_more_text_two']);
				$current_options['home_support_learn_more_link_two'] = sanitize_text_field($_POST['home_support_learn_more_link_two']);
				
				$current_options['home_support_icon_three'] = sanitize_text_field($_POST['home_support_icon_three']);
				$current_options['home_support_title_three'] = sanitize_text_field($_POST['home_support_title_three']);
				$current_options['home_support_desciption_three'] = sanitize_text_field($_POST['home_support_desciption_three']);
				$current_options['home_support_learn_more_text_three'] = sanitize_text_field($_POST['home_support_learn_more_text_three']);
				$current_options['home_support_learn_more_link_three'] = sanitize_text_field($_POST['home_support_learn_more_link_three']);
				$current_options['home_theme_support_bg'] = sanitize_text_field($_POST['home_theme_support_bg']);
				
				if(isset($_POST['home_support_learn_more_target_one']))
				{ echo $current_options['home_support_learn_more_target_one']=sanitize_text_field($_POST['home_support_learn_more_target_one']); } 
				else
				{ echo $current_options['home_support_learn_more_target_one']="off"; } 
				
				if(isset($_POST['home_support_learn_more_target_two']))
				{ echo $current_options['home_support_learn_more_target_two']=sanitize_text_field($_POST['home_support_learn_more_target_two']); } 
				else
				{ echo $current_options['home_support_learn_more_target_two']="off"; } 
				
				if(isset($_POST['home_support_learn_more_target_three']))
				{ echo $current_options['home_support_learn_more_target_three']=sanitize_text_field($_POST['home_support_learn_more_target_three']); } 
				else
				{ echo $current_options['home_support_learn_more_target_three']="off"; } 
				
				update_option('corpbiz_options', stripslashes_deep($current_options));
			}
		}	
		if($_POST['webriti_settings_save_15'] == 2) 
		{
			// Theme help and support section
			$current_options['home_theme_support_title'] ='We are here to help you';
			$current_options['home_theme_support_description'] ='24+7 hours support by us';
			
			$current_options['home_support_icon_one'] ='fa-meh-o';
			$current_options['home_support_title_one'] ='Need Support?';
			$current_options['home_support_desciption_one'] ='Lorem ipsum dolor sit amet, consectetur faucibus risus non iaculis. Fusce a augue Fusce in turpis in';
			$current_options['home_support_learn_more_text_one'] ='learn more';
			$current_options['home_support_learn_more_link_one'] ='#';
			$current_options['home_support_learn_more_target_one'] ='on';
			
			$current_options['home_support_icon_two'] ='fa-list-ol';
			$current_options['home_support_title_two'] ='Check Our Forum';
			$current_options['home_support_desciption_two'] ='Lorem ipsum dolor sit amet, consectetur faucibus risus non iaculis. Fusce a augue Fusce in turpis in';
			$current_options['home_support_learn_more_text_two'] ='learn more';
			$current_options['home_support_learn_more_link_two'] ='#';
			$current_options['home_support_learn_more_target_two'] ='on';
			
			$current_options['home_support_icon_three'] ='fa-support';
			$current_options['home_support_title_three'] ='Get Updated';
			$current_options['home_support_desciption_three'] ='Lorem ipsum dolor sit amet, consectetur faucibus risus non iaculis. Fusce a augue Fusce in turpis in';
			$current_options['home_support_learn_more_text_three'] ='learn more';
			$current_options['home_support_learn_more_link_three'] ='#';
			$current_options['home_support_learn_more_target_three'] ='on';
			$current_options['home_theme_support_bg'] ='';
			
			
			
			update_option('corpbiz_options',$current_options);
		}
	}  ?>
	<form method="post" id="webriti_theme_options_15">
		<div id="heading">
			<table style="width:100%;"><tr>
				<td><h2><?php _e('Theme Support Settings ','corpbiz');?></h2></td>
				<td><div class="webriti_settings_loding" id="webriti_loding_15_image"></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_15_success" ><?php _e('Options data successfully Saved','corpbiz');?></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_15_reset" ><?php _e('Options data successfully reset','corpbiz');?></div>
				</td>
				<td style="text-align:right;">
					<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('15');">
					<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('15')" >
				</td>
				</tr>
			</table>	
		</div>		
		<?php wp_nonce_field('webriti_customization_nonce_gernalsetting','webriti_gernalsetting_nonce_customization'); ?>
		<div class="section">
			<h3><?php _e('Theme Support Background','corpbiz'); ?></h3>
			<input class="webriti_inpute" type="text" value="<?php if($current_options['home_theme_support_bg']!='') { echo esc_attr($current_options['home_theme_support_bg']); } ?>" id="home_theme_support_bg" name="home_theme_support_bg" size="36" class="upload has-file"/>
			<input type="button" id="upload_button" value="Upload Background Image" class="upload_image_button" />
		</div>
		<div class="section">		
			<h3><?php _e('Title','corpbiz'); ?></h3>			
			<input class="webriti_inpute"  type="text" name="home_theme_support_title" id="home_theme_support_title" value="<?php echo $current_options['home_theme_support_title']; ?>" >
			<span class="explain"><?php _e('Enter the Theme Support Title.','corpbiz'); ?></span>
		</div>
		<div class="section">		
			<h3><?php _e('Short Description','corpbiz'); ?></h3>			
			<input class="webriti_inpute"  type="text" name="home_theme_support_description" id="home_theme_support_description" value="<?php echo $current_options['home_theme_support_description']; ?>" >
			<span class="explain"><?php _e('Enter the theme support short description.','corpbiz'); ?></span>
		</div>
		
		<div class="section">
			<h3><?php _e('Theme Support section One','corpbiz'); ?></h3>
			<hr>			
			<h3><?php _e('Icon','corpbiz'); ?>  </h3>			
			<input class="webriti_inpute"  type="text" name="home_support_icon_one" id="home_support_icon_one" value="<?php echo $current_options['home_support_icon_one']; ?>" >
			<span class="explain"><?php _e('(Using Font Awesome icons name) like: fa-meh-o .','corpbiz'); ?> <span style="margin-left:10px;"><a href="http://fontawesome.io/icons/" target="_blank"><?php _e('Get your fontawesome icons','corpbiz'); ?>.</a></span></span>
			
			<h3><?php _e('Title','corpbiz'); ?></h3>			
			<input class="webriti_inpute"  type="text" name="home_support_title_one" id="home_support_title_one" value="<?php echo $current_options['home_support_title_one']; ?>" >
			<span class="explain"><?php _e('Enter the theme support section one title.','corpbiz'); ?></span>
		
			<h3><?php _e('Description','corpbiz'); ?></h3>			
			<textarea rows="3" cols="8" id="home_support_desciption_one" name="home_support_desciption_one"><?php if($current_options['home_support_desciption_one']!='') { echo esc_attr($current_options['home_support_desciption_one']); } ?></textarea>
			<span class="explain"><?php _e('Enter the Learn More Text.','corpbiz'); ?></span>
			
			<h3><?php _e('Learn More Text','corpbiz'); ?></h3>			
			<input class="webriti_inpute"  type="text" name="home_support_learn_more_text_one" id="home_support_learn_more_text_one" value="<?php echo $current_options['home_support_learn_more_text_one']; ?>" >
			<span class="explain"><?php _e('Enter the theme support section one title.','corpbiz'); ?></span>
		
			<h3><?php _e('Learn More Link','corpbiz'); ?></h3>
			<p><input type="checkbox" id="home_support_learn_more_target_one" name="home_support_learn_more_target_one" <?php if($current_options['home_support_learn_more_target_one']=='on') echo "checked='checked'"; ?> > <?php _e('Open link in a new window/tab','corpbiz'); ?></p>
			<input class="webriti_inpute"  type="text" name="home_support_learn_more_link_one" id="home_support_learn_more_link_one" value="<?php echo $current_options['home_support_learn_more_link_one']; ?>" >
			<span class="explain"><?php _e('Enter the Learn More Link.','corpbiz'); ?></span>
		</div>	
		<div class="section">
			<h3><?php _e('Theme Support section Two','corpbiz'); ?></h3>
			<hr>			
			<h3><?php _e('Icon','corpbiz'); ?>  </h3>			
			<input class="webriti_inpute"  type="text" name="home_support_icon_two" id="home_support_icon_two" value="<?php echo $current_options['home_support_icon_two']; ?>" >
			<span class="explain"><?php _e('(Using Font Awesome icons name) like: fa-meh-o .','corpbiz'); ?> <span style="margin-left:10px;"><a href="http://fontawesome.io/icons/" target="_blank"><?php _e('Get your fontawesome icons','corpbiz'); ?>.</a></span></span>
			
			<h3><?php _e('Title','corpbiz'); ?></h3>			
			<input class="webriti_inpute"  type="text" name="home_support_title_two" id="home_support_title_two" value="<?php echo $current_options['home_support_title_two']; ?>" >
			<span class="explain"><?php _e('Enter the theme support section two title.','corpbiz'); ?></span>
		
			<h3><?php _e('Description','corpbiz'); ?></h3>			
			<textarea rows="3" cols="8" id="home_support_desciption_two" name="home_support_desciption_two"><?php if($current_options['home_support_desciption_two']!='') { echo esc_attr($current_options['home_support_desciption_two']); } ?></textarea>
			<span class="explain"><?php _e('Enter the Learn More Text.','corpbiz'); ?></span>
			
			<h3><?php _e('Learn More Text','corpbiz'); ?></h3>			
			<input class="webriti_inpute"  type="text" name="home_support_learn_more_text_two" id="home_support_learn_more_text_two" value="<?php echo $current_options['home_support_learn_more_text_two']; ?>" >
			<span class="explain"><?php _e('Enter the theme support section two title.','corpbiz'); ?></span>
		
			<h3><?php _e('Learn More Link','corpbiz'); ?></h3>
			<p><input type="checkbox" id="home_support_learn_more_target_two" name="home_support_learn_more_target_two" <?php if($current_options['home_support_learn_more_target_two']=='on') echo "checked='checked'"; ?> > <?php _e('Open link in a new window/tab','corpbiz'); ?></p>
			<input class="webriti_inpute"  type="text" name="home_support_learn_more_link_two" id="home_support_learn_more_link_two" value="<?php echo $current_options['home_support_learn_more_link_two']; ?>" >
			<span class="explain"><?php _e('Enter the Learn More Link.','corpbiz'); ?></span>
		</div>
		
		<div class="section">
			<h3><?php _e('Theme Support section Three','corpbiz'); ?></h3>
			<hr>			
			<h3><?php _e('Icon','corpbiz'); ?>  </h3>			
			<input class="webriti_inpute"  type="text" name="home_support_icon_three" id="home_support_icon_three" value="<?php echo $current_options['home_support_icon_three']; ?>" >
			<span class="explain"><?php _e('(Using Font Awesome icons name) like: fa-meh-o .','corpbiz'); ?> <span style="margin-left:10px;"><a href="http://fontawesome.io/icons/" target="_blank"><?php _e('Get your fontawesome icons','corpbiz'); ?>.</a></span></span>
			
			<h3><?php _e('Title','corpbiz'); ?></h3>			
			<input class="webriti_inpute"  type="text" name="home_support_title_three" id="home_support_title_three" value="<?php echo $current_options['home_support_title_three']; ?>" >
			<span class="explain"><?php _e('Enter the theme support section one title.','corpbiz'); ?></span>
		
			<h3><?php _e('Description','corpbiz'); ?></h3>			
			<textarea rows="3" cols="8" id="home_support_desciption_three" name="home_support_desciption_three"><?php if($current_options['home_support_desciption_three']!='') { echo esc_attr($current_options['home_support_desciption_three']); } ?></textarea>
			<span class="explain"><?php _e('Enter the Learn More Text.','corpbiz'); ?></span>
			
			<h3><?php _e('Learn More Text','corpbiz'); ?></h3>			
			<input class="webriti_inpute"  type="text" name="home_support_learn_more_text_three" id="home_support_learn_more_text_three" value="<?php echo $current_options['home_support_learn_more_text_three']; ?>" >
			<span class="explain"><?php _e('Enter the theme support section one title.','corpbiz'); ?></span>
		
			<h3><?php _e('Learn More Link','corpbiz'); ?></h3>
			<p><input type="checkbox" id="home_support_learn_more_target_three" name="home_support_learn_more_target_three" <?php if($current_options['home_support_learn_more_target_three']=='on') echo "checked='checked'"; ?> > <?php _e('Open link in a new window/tab','corpbiz'); ?></p>
			<input class="webriti_inpute"  type="text" name="home_support_learn_more_link_three" id="home_support_learn_more_link_three" value="<?php echo $current_options['home_support_learn_more_link_three']; ?>" >
			<span class="explain"><?php _e('Enter the Learn More Link.','corpbiz'); ?></span>
		</div>
		
		<div id="button_section">
			<input type="hidden" value="1" id="webriti_settings_save_15" name="webriti_settings_save_15" />
			<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('15');">
			<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('15')" >
		</div>
		<div class="webriti_spacer"></div>
	</form>
</div>