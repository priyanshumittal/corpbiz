<?php 
$current_options = wp_parse_args(  get_option( 'corpbiz_options', array() ), theme_data_setup() );
if(isset($_POST['webriti_settings_save_6']))
{	
	if($_POST['webriti_settings_save_6'] == 1) 
	{	$current_options['client_list']= sanitize_text_field($_POST['client_list']);
		$current_options['home_client_title']= sanitize_text_field($_POST['home_client_title']);
		$current_options['home_client_desciption']=sanitize_text_field($_POST['home_client_desciption']);
		
		update_option('corpbiz_options', stripslashes_deep($current_options));
	}
	if($_POST['webriti_settings_save_6'] == 2) 
	{	
		$current_options['client_list']='4';					
		$current_options['home_client_title']= 'Meet Our Greatest Client';
		$current_options['home_client_desciption']='We Love Our Clients.';					
		update_option('corpbiz_options',$current_options);
	}
} // end of submit client list
?>
<div class="block ui-tabs-panel deactive" id="option-ui-id-6" >
	<form method="post" id="webriti_theme_options_6">
	<?php //print_r($current_options['client_list']); ?>
		<div id="heading">			
			<table style="width:100%;">
				<tr><td><h2><?php _e('Client','corpbiz');?></h2></td>
					<td style="width:30%;">
						<div class="webriti_settings_loding" id="webriti_loding_6_image"></div>
						<div class="webriti_settings_massage" id="webriti_settings_save_6_success" ><?php _e('Options data successfully Saved','corpbiz');?></div>
						<div class="webriti_settings_massage" id="webriti_settings_save_6_reset" ><?php _e('Options data successfully reset','corpbiz');?></div>
					</td>
					<td style="text-align:right;">
						<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('6');">
						<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('6')" >
					</td>
				</tr>				
			</table>	
		</div>		
		<div class="section">
			<h3><?php _e('Home Page client Heading','corpbiz');?></h3>
			<input class="webriti_inpute"  type="text" name="home_client_title" id="home_client_title" value="<?php if($current_options['home_client_title']!='') { echo esc_attr($current_options['home_client_title']); } ?>" >		
			<span class="explain"><?php  _e('Enter Heading for client Section.','corpbiz');?></span>
			<h3><?php _e('Home Page Client section description','corpbiz');?></h3>
			<input class="webriti_inpute"  type="text" name="home_client_desciption" id="home_client_desciption" value="<?php if($current_options['home_client_desciption']!='') { echo esc_attr($current_options['home_client_desciption']); } ?>" >		
			<span class="explain"><?php  _e('Enter Heading for client Section.','corpbiz');?></span>
		</div>
		<div class="section">
			<h3><?php _e('Number of client on client/testimonial section','corpbiz');?></h3>
			<?php $client_list = $current_options['client_list']; ?>		
			<select name="client_list" class="webriti_inpute" >					
				<option value="2" <?php selected($client_list, '2' ); ?>>2</option>
				<option value="4" <?php selected($client_list, '4' ); ?>>4</option>
				<option value="6" <?php selected($client_list, '6' ); ?>>6</option>
				<option value="8" <?php selected($client_list, '8' ); ?>>8</option>
				<option value="10" <?php selected($client_list, '10' ); ?>>10</option>
				<option value="12" <?php selected($client_list, '12' ); ?>>12</option>
				<option value="14" <?php selected($client_list, '14' ); ?>>14</option>
				<option value="16" <?php selected($client_list, '16' ); ?>>16</option>
				<option value="18" <?php selected($client_list, '18' ); ?>>18</option>
				<option value="20" <?php selected($client_list, '20' ); ?>>20</option>
				<option value="22" <?php selected($client_list, '22' ); ?>>22</option>
			</select>
			<span class="explain"><?php  _e('Select your number of clients','corpbiz');?></span>
		</div>
		
		<div id="button_section">
			<input type="hidden" value="1" id="webriti_settings_save_6" name="webriti_settings_save_6" />
			<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('6');">
			<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('6')" >
		</div>		
	</form>	
</div>