<div class="wrap" id="framework_wrap">   		
    <div id="content_wrap">
		<div class="webriti-header">
			<h2 style="padding-top: 0px;font-size: 23px;line-height: 10px;"><a href="http://www.webriti.com/" style="margin-bottom:0px;"><img style="margin-left:10px;" src="<?php echo get_template_directory_uri(); ?>/functions/theme_options/images/png.png"></a></h2>
		</div>
		<div class="webriti-submenu">
		<div id="icon-themes" class="icon32"></div>
			<h2><?php _e('Corpbiz','corpbiz'); ?>
				<div class="webriti-submenu-links">
					<a target="_blank" href="http://webriti.com/support/categories/corpbiz" class="btn btn-primary"><?php _e('Support Desk','corpbiz'); ?></a>
					<a target="_blank" href="http://webriti.com/themes/documentation/corpbiz/" class="btn btn-info"> <?php _e('Theme Documentation','corpbiz'); ?></a>
					<a target="_blank" href="http://webriti.com/support/discussion/355/corpbiz-change-log/" class="btn btn-success" ><?php _e('View Changelog','corpbiz'); ?></a>				
				</div><!-- webriti-submenu-links -->
			</h2>
          <div class="clear"></div>
        </div>
        <div id="content">
			<div id="options_tabs" class="ui-tabs ">
				<ul class="options_tabs ui-tabs-nav" role="tablist" id="nav">
					<div id="nav-shadow"></div>
					<li class="active" >
						<div class="arrow"><div></div></div><a href="#" id="1"><span class="icon home-page"></span><?php _e('Home Page','corpbiz'); ?></a>
						<ul><li class="currunt" ><a href="#" class="ui-tabs-anchor" id="ui-id-1"><?php _e('Quick Start','corpbiz'); ?> </a><span></span></li>
							<li><a href="#"  id="ui-id-2"><?php _e('Slider Setting','corpbiz'); ?></a><span></span></li>
							<li><a href="#"  id="ui-id-3"><?php _e('Site Info Setting','corpbiz'); ?></a><span></span></li> 
							<li><a href="#"  id="ui-id-14"><?php _e('Service Setting','corpbiz'); ?></a><span></span></li>			
							<li><a href="#"  id="ui-id-4"><?php _e('Portfolio Setting','corpbiz'); ?></a><span></span></li>
							<li><a href="#"  id="ui-id-6"><?php _e('Our Clients Settings','corpbiz'); ?></a><span></span></li>
							<li><a href="#"  id="ui-id-15"><?php _e('Theme Support Settings','corpbiz'); ?></a><span></span></li>
							<li><a href="#"  id="ui-id-5"><?php _e('Footer call out area','corpbiz'); ?></a><span></span></li>
						</ul>
					</li>
					<li>
						<div class="arrow"><div></div></div><a href="#" id="ui-id-12"><span class="icon typography"></span><?php _e('Typography','corpbiz'); ?></a><span></span>
					</li>				
					<li>
						<div class="arrow"><div></div></div><a href="#" id="7"><span class="icon contact-page"></span><?php _e('Contact Page','corpbiz'); ?></a>
						<ul><li ><a href="#" class="ui-tabs-anchor" id="ui-id-7"><?php _e('Contact Information','corpbiz'); ?> </a><span></span></li>
							<li><a href="#"  id="ui-id-9"><?php _e('Google Maps','corpbiz'); ?></a><span></span></li>
						</ul>
					</li>
					<li>
						<div class="arrow"><div></div></div><a href="#" id="ui-id-10"><span class="icon home_layout_manger"></span><?php _e('Layout Manager','corpbiz'); ?></a><span></span>
					</li>	
					<li>
						<div class="arrow"><div></div></div><a href="#" id="ui-id-11"><span class="icon footer"></span><?php _e('Footer Customization','corpbiz'); ?></a><span></span>
					</li>
					<li>
						<div class="arrow"><div></div></div><a href="#" id="ui-id-16"><span class="icon footer"></span><?php _e('Taxonomy Archive Portfolio','corpbiz'); ?></a><span></span>
					</li>
					<div id="nav-shadow"></div>
                </ul>
				<!--         Home Page   -------->
				<!--most 1 tabs home_page_settings --> 
				<?php require_once('pages/home_page_settings.php'); ?>				
				
				<!--most 2 tabs home_page_settings --> 
				<?php require_once('pages/home_slider_settings.php'); ?>				
				
				<!--most 3 home_service_settings tabs s --> 
				<?php require_once('pages/home_site_info_settings.php'); ?>				
				
				<!--most 4 tabs home_project_portfolio_settings --> 
				<?php require_once('pages/home_project_portfolio_settings.php'); ?>				
				
				<!--most 14 tabs home_project_portfolio_settings --> 
				<?php require_once('pages/home_service_settings.php'); ?>
				
				<!--most 15 tabs home_project_portfolio_settings --> 
				<?php require_once('pages/home_theme_support_settings.php'); ?>
				
				<!--most 5 tabs home_page_settings --> 
				<?php require_once('pages/home_call_out_settings.php'); ?>

				<!--most 6 Home page Clients -->
				<?php require_once('pages/option_clients_settings.php'); ?>
				
				<!--most 12 tabs home_page_settings --> 
				<?php require_once('pages/typography.php'); ?>
				
				<!--most 7 tabs home_page_settings --> 
				<?php require_once('pages/contact_page_settings.php'); ?>
				<!--most 9 tabs home_page_settings --> 
				<?php require_once('pages/google_maps_settings.php'); ?>				
				
				<!--most 10 tabs home_page_settings --> 
				<?php require_once('pages/home_layout_manager.php'); ?>				
				<!--most 11 tabs home_page_settings --> 
				<?php require_once('pages/footer_customization_settings.php'); ?>

				<!--most 16 tabs home_page_settings --> 
				<?php require_once('pages/taxonomy_archive_portfolio_setting.php'); ?>
				
			</div>		
        </div>
		<div class="webriti-submenu" style="height:35px;">			
            <div class="webriti-submenu-links" style="margin-top:5px;">
			<form method="POST">
				<input type="submit" onclick="return confirm( 'Click OK to reset theme data. Theme settings will be lost!' );" value="Restore All Defaults" name="restore_all_defaults" id="restore_all_defaults" class="reset-button btn">
			<form>
            </div><!-- webriti-submenu-links -->
        </div>
		<div class="clear"></div>
    </div>
</div>
<?php
// Restore all defaults
if(isset($_POST['restore_all_defaults'])) 
	{
		$corpbiz_theme_options = theme_data_setup();	
		update_option('corpbiz_options',$corpbiz_theme_options);
	}
?>