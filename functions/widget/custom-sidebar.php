<?php	
add_action( 'widgets_init', 'webriti_widgets_init');
function webriti_widgets_init() {
/*sidebar*/
register_sidebar( array(
		'name' => __( 'Sidebar', 'corpbiz' ),
		'id' => 'sidebar-primary',
		'description' => __( 'The primary widget area', 'corpbiz' ),
		'before_widget' => '<div class="sidebar_widget" >',
		'after_widget' => '</div>',
		'before_title' => '<div class="sidebar_widget_title"><h2>',
		'after_title' => '</h2></div>',
	) );

register_sidebar( array(
		'name' => __( 'Footer Widget Area', 'corpbiz' ),
		'id' => 'footer-widget-area',
		'description' => __( 'footer widget area', 'corpbiz' ),
		'before_widget' => '<div class="col-md-4 col-sm-6 footer_widget_column">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="footer_widget_title">',
		'after_title' => '</h2>',
	) );
}                     
?>