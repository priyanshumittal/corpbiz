<?php
add_action( 'widgets_init','corpbiz_latest_widget'); 
   function corpbiz_latest_widget() { return   register_widget( 'corpbiz_latest_widget' ); }

/**
 * Adds corpbiz_latest_widget widget.
 */
class corpbiz_latest_widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'corpbiz_latest_widget', // Base ID
			__('Corpbiz Latest Blog', 'corpbiz'), // Name
			array( 'description' => __( 'This widget show latest post and comment or popular blog', 'corpbiz' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance) {
		$title = apply_filters( 'widget_title', $instance['title'] );
			echo $args['before_widget'];
			if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title']; ?>
				<ul class="sidebar_tab sidebar_widget_tab">
					<li class="active"><a data-toggle="tab" href="#popular"><?php _e('Popular','corpbiz'); ?></a></li>
					<li><a data-toggle="tab" href="#recent"><?php _e('Recent','corpbiz'); ?></a></li>
					<li><a data-toggle="tab" href="#comment"><?php _e('Comment','corpbiz'); ?></a></li>
				</ul>				
				<div class="tab-content" id="myTabContent">					
					<div id="popular" class="tab-pane fade active in">
						<div class="row">	
						<?php global $wpdb;
							$pop = $wpdb->get_results("SELECT id,guid,post_date,post_title, comment_count FROM {$wpdb->prefix}posts WHERE post_type='post' AND post_status='publish' ORDER BY comment_count DESC LIMIT 5");
							foreach($pop as $post): ?>
								<div class="media post_media_sidebar">
									<a class="pull-left " href="#">
										<?php $atts=array('class' => 'img-responsive post_sidebar_img'	);?>
										<?php echo get_the_post_thumbnail( $post->id,'webriti_sidebar_thumb', $atts); ?>
									</a>
									<div class="media-body">
										<h3 style="padding-bottom:0px;"><a href="<?php echo $post->guid; ?>"><?php echo $post->post_title; ?></a></h3>
										<p><?php echo get_sidebar_excerpt(); ?></p>
									</div>
									<div class="sidebar_comment_box">
										<span>By admin <a href="<?php echo get_author_posts_url( get_the_author_meta( $post->id ) ); ?>"><i class="fa fa-circle"></i> <?php comments_number('No Comments', '1 Comment','% Comments'); ?></a></span>
									</div>									
								</div>							
							<?php endforeach; ?>
						</div>
					</div>
					<div id="recent" class="tab-pane fade">
						<div class="row">
						<?php 	
							$args = array( 'post_type' => 'post','posts_per_page' =>5, 'post__not_in'=>get_option("sticky_posts")); 	
								query_posts( $args );
								if(query_posts( $args ))
								{	while(have_posts()):the_post();
										{ ?>
									<div class="media post_media_sidebar">
										<a class="pull-left " href="#">
											<?php
												$defalt_arg =array('class' => "img-responsive post_sidebar_img");
												if(has_post_thumbnail()): 
												the_post_thumbnail('webriti_sidebar_thumb',$defalt_arg); 
												endif; 
												?>
										</a>
										<div class="media-body">
											<h3 style="padding-bottom:0px;"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
											<p><?php echo get_sidebar_excerpt(); ?></p>
										</div>
										<div class="sidebar_comment_box">
											<span>By admin <a href="<?php echo get_author_posts_url( get_the_author_meta( $post->id ) ); ?>"><i class="fa fa-circle"></i> <?php comments_number('No Comments', '1 Comment','% Comments'); ?></a></span>
										</div>									
									</div>
									<?php
									} endwhile; 
								} ?>						
						</div>	
					</div>
					<div id="comment" class="tab-pane fade">
						<div class="row"><?php	
							$args = array('number' => '5');
							$comments = get_comments($args);
							foreach($comments as $comment) 
							{	$pop1 = $wpdb->get_results("SELECT guid  FROM {$wpdb->prefix}posts WHERE id='$comment->comment_post_ID' ORDER BY comment_count DESC LIMIT 5");
								foreach($pop1 as $post1) { ?>
									<div class="media post_media_sidebar">
										<a class="pull-left sidebar_pull_img" href="<?php echo $post1->guid; ?>">
											<?php echo get_avatar($comment, 70 ); ?>
										</a>
										<div class="media-body">
											<h3 style="padding-bottom:0px;"><a href="#"><?php  echo$comment->comment_author; ?></a></h3>
											<p><?php echo get_comment_sidebr($comment->comment_content); ?></p>
											<p><a href="<?php echo get_permalink($comment->comment_post_ID); ?>#comment-<?php echo $comment->comment_ID; ?>" class="readmore">Read More</a></p>
										</div>										
									</div>
								<?php 
								}  
							}  ?>									
						</div>					
					</div>
					
				</div>
				</div>
				
	<?php	
		//echo $args['after_widget']; // end of contact widget
	}
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}else {	$title ='Tabs Content'; } ?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:','busi_prof' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php 
	}
	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
} // class Foo_Widget
?>