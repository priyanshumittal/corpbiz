<?php
//code to change length of Home service section excerpt
	function get_sidebar_excerpt(){
		global $post;
		$excerpt = get_the_content();
		$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
		$excerpt = strip_shortcodes($excerpt);		
		$original_len = strlen($excerpt);
		$excerpt = substr($excerpt, 0, 45);		
		$len=strlen($excerpt);
		if($original_len>45)
		   $excerpt = $excerpt.'<p><a class="readmore" href="'. get_permalink($post->ID) . '"> Read More</a></p>';
		return $excerpt;
	}
	function get_comment_sidebr($excerpt){
		
		$excerpt = $excerpt;
		$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
		$excerpt = strip_shortcodes($excerpt);		
		$original_len = strlen($excerpt);
		$excerpt = substr($excerpt, 0, 45);		
		return $excerpt;
	}
	
?>