<?php
/*
* @Theme Name	:	Corpbiz-Pro
* @file         :	taxonomies.php
* @package      :	Corpbiz-Pro
* @author       :	Hari Maliya
* @license      :	license.txt* 
 * Add custom taxonomies
 * Additional custom taxonomies can be defined here
 * http://codex.wordpress.org/Function_Reference/register_taxonomy
 */
function create_portfolio_taxonomy() {
    register_taxonomy('cor_portfolio_categories', 'corpbiz_portfolio',
    array(  'hierarchical' => true,
			'show_in_nav_menus' => true,
            'label' => 'Portfolio Categories',
            'query_var' => true));
	//Default category id		
	$defualt_tex_id = get_option('custom_texo_corpbiz');
	
	//quick update category
	if((isset($_POST['action'])) && (isset($_POST['taxonomy']))){		
		wp_update_term($_POST['tax_ID'], 'cor_portfolio_categories', array(
		  'name' => $_POST['name'],
		  'slug' => $_POST['slug']
		));	
		update_option('custom_texo_corpbiz', $defualt_tex_id);
	} 
	else 
	{ 	//insert default category 
		if(!$defualt_tex_id){
			wp_insert_term('ALL','cor_portfolio_categories', array('description'=> 'Default Category','slug' => 'ALL'));
			$Current_text_id = term_exists('ALL', 'cor_portfolio_categories');
			update_option('custom_texo_corpbiz', $Current_text_id['term_id']);
		}
	}
	//update category
	if(isset($_POST['submit']) && isset($_POST['action']) )
	{	wp_update_term($_POST['tag_ID'], 'cor_portfolio_categories', array(
		  'name' => $_POST['name'],
		  'slug' => $_POST['slug'],
		  'description' =>$_POST['description']
		));
	}
	// Delete default category
	if(isset($_POST['action']) && isset($_POST['tag_ID']) )
	{	if(($_POST['tag_ID'] == $defualt_tex_id) &&$_POST['action']	 =="delete-tag")
		{	
			delete_option('custom_texo_corpbiz'); 
		} 
	}	
	
}
add_action( 'init', 'create_portfolio_taxonomy' );
?>