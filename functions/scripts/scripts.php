<?php
function webriti_scripts()
{	
	//wp_enqueue_style('nimble-slider-style', WEBRITI_TEMPLATE_DIR_URI . '/css/nimble-slider-style.css');	
	wp_enqueue_style( 'corpbiz-style', get_stylesheet_uri() );
	wp_enqueue_style('bootstrap-css', WEBRITI_TEMPLATE_DIR_URI . '/css/bootstrap.css');
	wp_enqueue_style('theme-menu', WEBRITI_TEMPLATE_DIR_URI . '/css/theme-menu.css');
	wp_enqueue_style('font-awesome-min','//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');	
	wp_enqueue_style('media-responsive', WEBRITI_TEMPLATE_DIR_URI . '/css/media-responsive.css');	
	
	wp_enqueue_script('menu', WEBRITI_TEMPLATE_DIR_URI .'/js/menu/menu.js',array('jquery'));
	wp_enqueue_script('bootstrap-min', WEBRITI_TEMPLATE_DIR_URI .'/js/bootstrap.min.js');
	
		
	// Portfolio js and css
	if(is_page_template('portfolio-2-column.php') || is_page_template('portfolio-3-column.php') || is_page_template('portfolio-4-column.php') || is_tax('cor_portfolio_categories'))
	{	wp_enqueue_style('lightbox', WEBRITI_TEMPLATE_DIR_URI . '/css/lightbox.css');	
		wp_enqueue_script('lightbox-2-6', WEBRITI_TEMPLATE_DIR_URI .'/js/lightbox/lightbox-2.6.min.js');
	}	
	if(is_front_page())
	{	
		wp_enqueue_style('lightbox', WEBRITI_TEMPLATE_DIR_URI . '/css/lightbox.css');
		wp_enqueue_script('lightbox-2-6', WEBRITI_TEMPLATE_DIR_URI .'/js/lightbox/lightbox-2.6.min.js');
		
		/*******nimble slider js*******/
		function slider_js_function() {
			wp_enqueue_script('jquerya4e6', WEBRITI_TEMPLATE_DIR_URI .'/js/flexslider/jquery.js');
			wp_enqueue_script('superfish-js', WEBRITI_TEMPLATE_DIR_URI .'/js/flexslider/superfish.js');
			wp_enqueue_script('custom-js', WEBRITI_TEMPLATE_DIR_URI .'/js/flexslider/custom.js');
			wp_enqueue_script('jquery-flexslider-min-js', WEBRITI_TEMPLATE_DIR_URI .'/js/flexslider/jquery.flexslider-min.js');
			wp_enqueue_script('jquery-carouFredSel-6-2-1', WEBRITI_TEMPLATE_DIR_URI .'/js/caroufredsel/jquery.carouFredSel-6.2.1-packed.js');
			wp_enqueue_script('caroufredsel-element', WEBRITI_TEMPLATE_DIR_URI .'/js/caroufredsel/caroufredsel-element.js');		
		
		}
		add_action('wp_footer', 'slider_js_function');

	}
	if(is_page_template('service-template.php'))
	{	
		wp_enqueue_script('jquery-carouFredSel-6-2-1', WEBRITI_TEMPLATE_DIR_URI .'/js/caroufredsel/jquery.carouFredSel-6.2.1-packed.js');
		wp_enqueue_script('caroufredsel-element', WEBRITI_TEMPLATE_DIR_URI .'/js/caroufredsel/caroufredsel-element.js');	
	}
	require_once('custom_style.php');
	
}
add_action('wp_enqueue_scripts', 'webriti_scripts');

if ( is_singular() ){ wp_enqueue_script( "comment-reply" );	}
function corpbiz_shortcode_detect() {
    global $wp_query;	
    $posts = $wp_query->posts;
    $pattern = get_shortcode_regex();
	foreach ($posts as $post){
        if (   preg_match_all( '/'. $pattern .'/s', $post->post_content, $matches ) && array_key_exists( 2, $matches ) && in_array( 'button', $matches[2] ) || in_array( 'row', $matches[2] ) || in_array( 'accordian', $matches[2] ) || in_array( 'tabgroup', $matches[2]) || in_array( 'tabs', $matches[2] ) || in_array( 'alert', $matches[2] ) || in_array( 'dropcap', $matches[2] )  || in_array( 'gridsystemlayout', $matches[2] ) || in_array( 'column', $matches[2] ) || in_array( 'heading', $matches[2] )) {
			//wp_enqueue_script('collapse', WEBRITI_TEMPLATE_DIR_URI .'/js/collapse.js');
			wp_enqueue_script('accordion-tab', WEBRITI_TEMPLATE_DIR_URI .'/js/accordion-tab.js',array('jquery'));			
			break;
        }
    }
}
add_action( 'wp', 'corpbiz_shortcode_detect' ); 
?>