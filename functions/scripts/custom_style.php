<?php $current_options=get_option('corpbiz_pro_options'); 
if($current_options['enable_custom_typography']=="on")
{
?>
<style> 
/****** custom typography *********/ 
 .homepage_service_section p,.Client_content p ,.content_responsive_section p,.blog_post_content p,.aboutus_buynow_section p, .about_description_area p,.blog_post_content p,.homepage_top_callout p
 {
	font-size:<?php echo $current_options['general_typography_fontsize'].'px'; ?> ;
	font-family:<?php echo $current_options['general_typography_fontfamily']; ?> ;
	font-style:<?php echo $current_options['general_typography_fontstyle']; ?> ;
	line-height:<?php echo ($current_options['general_typography_fontsize']+5).'px'; ?> ;
	
}
/*** Menu title */
.navbar .navbar-nav > li > a{
	font-size:<?php echo $current_options['menu_title_fontsize'].'px' ; ?> !important;
	font-family:<?php echo $current_options['menu_title_fontfamily']; ?> !important;
	font-style:<?php echo $current_options['menu_title_fontstyle']; ?> !important;
}
/*** post and Page title */
.post_title_wrapper h2 a {
	font-size:<?php echo $current_options['post_title_fontsize'].'px'; ?>;
	font-family:<?php echo $current_options['post_title_fontfamily']; ?>;
	font-style:<?php echo $current_options['post_title_fontstyle']; ?>;
}
/*** service title */
.homepage_service_section h2
{
	font-size:<?php echo $current_options['service_title_fontsize'].'px'; ?>;
	font-family:<?php echo $current_options['service_title_fontfamily']; ?>;
	font-style:<?php echo $current_options['service_title_fontstyle']; ?>;
}

/******** portfolio title ********/
.corpo_home_portfolio_showcase_icons h4,.portfolio_caption a { 
	font-size:<?php echo $current_options['portfolio_title_fontsize'].'px'; ?>;
	font-family:<?php echo $current_options['portfolio_title_fontfamily']; ?>;
	font-style:<?php echo $current_options['portfolio_title_fontstyle']; ?>;
}
/******* footer widget title*********/
.footer_widget_title, .sidebar_widget_title h2{
	font-size:<?php echo $current_options['widget_title_fontsize'].'px'; ?>;
	font-family:<?php echo $current_options['widget_title_fontfamily']; ?>;
	font-style:<?php echo $current_options['widget_title_fontstyle']; ?>;
}
.footer_callout_area h2{
	font-size:<?php echo $current_options['calloutarea_title_fontsize'].'px'; ?>;
	font-family:<?php echo $current_options['calloutarea_title_fontfamily']; ?>;
	font-style:<?php echo $current_options['calloutarea_title_fontstyle']; ?>;
}
.footer_callout_area p{
	font-size:<?php echo $current_options['calloutarea_description_fontsize'].'px'; ?>;
	font-family:<?php echo $current_options['calloutarea_description_fontfamily']; ?>;
	font-style:<?php echo $current_options['calloutarea_description_fontstyle']; ?>;
}
</style>
<?php } ?>