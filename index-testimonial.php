<!--Homepage Clients Section-->
<?php $current_options = get_option('corpbiz_options',theme_data_setup()); ?>
<div class="container">
	<div class="row">
		<div class="corpo_heading_title">
			<?php if($current_options['home_client_title'] !="") { ?>
			<h1><?php echo $current_options['home_client_title']; ?></h1>
			<?php } ?>
			<?php if($current_options['home_client_desciption'] !="") { ?>
			<p><?php echo $current_options['home_client_desciption']; ?></p>
			<?php } ?>
		</div>	
	</div>
	<div class="row">	
		<div class="media_row">
			<?php  
			$total_client = $current_options['client_list'];
			$count_posts = wp_count_posts('corpbiz_testimonial')->publish;			
			if($count_posts>$total_client)
			{
				$count_posts = $total_client;
			}
			else
			{ 	$count_posts = $count_posts;  }
			
			$client_row = intval($count_posts / 2);
			$client_row = ($client_row*2);			
			if($count_posts%2==0)
			{	$start = $client_row-1;
				$end = $client_row;
			}
			else
			{	$start = $client_row+1;
				$end = $count_posts;
			}
			$i=1;
			$args = array( 'post_type' => 'corpbiz_testimonial','posts_per_page' =>$count_posts); 	
			$corpbiz_testimonial = new WP_Query( $args );
			if( $corpbiz_testimonial->have_posts() )
			{ 	while ( $corpbiz_testimonial->have_posts() ) : $corpbiz_testimonial->the_post(); ?>
				<div style="border-bottom:<?php if($i >$client_row) { echo "none"; } else { if($i >=$start && $i<=$end) { echo "none"; } } ?>;" class="Client_area media_column media_padding_top <?php  if($i%2) { echo "media_padding_right"; } else { echo "media_border_right media_padding_left"; }  ?>">
					<div class="client_box">
						<?php if(has_post_thumbnail()): ?>
						<?php $defalt_arg =array('class' => "img-responsive"); ?>
						<?php the_post_thumbnail('', $defalt_arg); ?>					
						<?php endif; ?>
					</div>
					<div class="media-body Client_content">
						<?php if(get_post_meta( get_the_ID(),'testimonial_description_meta', true )){ ?>
						<p><?php echo get_post_meta( get_the_ID(),'testimonial_description_meta', true ); ?></p>
						<?php } ?>
						<a <?php if(get_post_meta( get_the_ID(),'testimonial_link_target_meta', true )) { echo 'target="_blank"'; } ?> href="<?php echo get_post_meta( get_the_ID(),'testimonial_link_meta', true ); ?>"><span><?php the_title(); ?></span></a>
						<?php 
						if(get_post_meta( get_the_ID(),'testimonial_author_designation_meta', true )){ ?>
						<small><?php echo get_post_meta( get_the_ID(),'testimonial_author_designation_meta', true ); ?></small>
						<?php } ?>
					</div>
				</div>			
			<?php if($i%2==0){ echo "<div class='clearfix'></div>"; } $i++; endwhile;
			} else { ?>
			<div class="Client_area media_column media_padding_right media_padding_top">
				<div class="pull-left client_box">
					<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/client1.jpg" alt="John Doe" />
				</div>
				<div class="media-body Client_content">
					<p><?php _e('Lorem ipsum dolor sit amet, consectetur adipiscin faucibus risus non iaculis. Fusce a augue ante, pel Fusce in turpis in velit tempor pretium.','corpbiz'); ?></p>
					<span><?php _e('John Doe','corpbiz'); ?></span>
					<small><?php _e('CEO (Scientech)','corpbiz'); ?></small>
				</div>
			</div>
			
			<div class="Client_area media_column media_border_right media_padding_left media_padding_top">
				<div class="pull-left client_box">
					<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/client1.jpg" alt="John Doe" />
				</div>
				<div class="media-body Client_content">
					<p><?php _e('Lorem ipsum dolor sit amet, consectetur adipiscin faucibus risus non iaculis. Fusce a augue ante, pel Fusce in turpis in velit tempor pretium.','corpbiz'); ?></p>
					<span><?php _e('John Doe','corpbiz'); ?></span>
					<small><?php _e('CEO (Scientech)','corpbiz'); ?></small>
				</div>
			</div>
			
			<div class="Client_area media_column media_border_bottom media_padding_right media_padding_top">
				<div class="pull-left client_box">
					<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/client1.jpg" alt="John Doe" />
				</div>
				<div class="media-body Client_content">
					<p><?php _e('Lorem ipsum dolor sit amet, consectetur adipiscin faucibus risus non iaculis. Fusce a augue ante, pel Fusce in turpis in velit tempor pretium.','corpbiz'); ?></p>
					<span><?php _e('John Doe','corpbiz'); ?></span>
					<small><?php _e('CEO (Scientech)','corpbiz'); ?></small>
				</div>
			</div>
			
			<div class="Client_area media_column media_border_bottom media_border_right media_padding_left media_padding_top">
				<div class="pull-left client_box">
					<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/client1.jpg" alt="John Doe" />
				</div>
				<div class="media-body Client_content">
					<p><?php _e('Lorem ipsum dolor sit amet, consectetur adipiscin faucibus risus non iaculis. Fusce a augue ante, pel Fusce in turpis in velit tempor pretium.','corpbiz'); ?></p>
					<span><?php _e('John Doe','corpbiz'); ?></span>
					<small><?php _e('CEO (Scientech)','corpbiz'); ?></small>
				</div>
			</div>
			<?php } ?>
		</div>		
	</div>
</div>
<!--/Homepage Clients Section-->