<?php
// Template Name: Contact-Us
 get_header(); 
get_template_part('index', 'banner');
$current_options = get_option('corpbiz_options',theme_data_setup()); ?>
<div class="container">	
	<div class="row contact_section">
	<?php the_post(); 		
				if(has_post_thumbnail()){
				$defalt_arg =array('class' => "img-responsive"); 
				the_post_thumbnail('', $defalt_arg); ?>
					<div class="about-section-space">
				</div>				
				<?php the_content(); ?>
				<?php }
				if($current_options['send_usmessage'] !='') { ?>
		<div class="col-md-9">
			<div class="row">
			 <div class="col-md-12">
			    <h2 class="contact_form_title"><?php echo $current_options['send_usmessage']; ?></h2>
				</div>
				<?php } ?>
			</div>
			<div id="myformdata">
			<form  method="POST" id="contact_form" name="contact_form" action="#">
				<div class="row">
					<div class="form-group">
						<div class="col-md-4">
							<label><?php _e('Name','corpbiz'); ?>*</label>
							<input type="text" name="user_name" id="user_name" class="contact_input_control">
							<span  style="display:none; color:red" id="contact_user_name_error"><?php _e('Please Enter Your name','corpbiz'); ?> </span>
						</div>
						<div class="col-md-4">
							<label><?php _e('Email Address','corpbiz'); ?>*</label>
							<input type="text" name="user_email" id="user_email" class="contact_input_control">
							<span  style="display:none; color:red" id="contact_user_email_error"><?php _e('Please Enter Your email','corpbiz'); ?> </span>
						</div>
						<div class="col-md-4">
							<label><?php _e('Subject','corpbiz'); ?></label>
							<input type="text" name="user_subject" id="user_subject" class="contact_input_control">
							<span  style="display:none; color:red" id="contact_user_subject_error"><?php _e('Please Enter Your subject','corpbiz'); ?> </span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-md-12">
							<label><?php _e('Message','corpbiz'); ?></label>
							<textarea name="user_massage" id="user_massage" class="contact_textarea_control" rows="7"></textarea>
							<span  style="display:none; color:red" id="contact_user_massage_error"><?php _e('Please Enter Your massage','corpbiz'); ?> </span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<button class="cont_btn btn_red" type="submit" name="contact_submit" ><?php _e('Send Message','corpbiz'); ?></button>
					</div>
				</div>
			</form>
			</div>
			<div id="mailsentbox" style="display:none">
				<div class="alert alert-success" >
					<strong><?php _e('Thank  you!','corpbiz');?></strong> <?php _e('You successfully sent contact information...','quality');?>
				</div>
			</div>
			
		</div>
		<?php
		if(isset($_POST['contact_submit']))
		{ 	
			if($_POST['user_name'] ==''){					
				echo "<script>jQuery('#contact_user_name_error').show();</script>";
			} 
			else 
			if($_POST['user_email']=='') {	
				echo "<script>jQuery('#contact_user_email_error').show();</script>";
			}
			else
			if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i",$_POST['user_email'])) {	
				echo "<script>jQuery('#contact_user_email_error').show();</script>";
			} 
			else
			if($_POST['user_subject'] == ''){	
				echo "<script>jQuery('#contact_user_subject_error').show();</script>";
			}
			else
			if($_POST['user_massage']=='')
			{
				echo "<script>jQuery('#contact_user_massage_error').show();</script>";
			}
			else
			{	
				$to = get_option('admin_email');
				$subject = trim($_POST['user_name']) . $_POST['user_subject'] . get_option("blogname");
				$massage = stripslashes(trim($_POST['user_massage']))."Message sent from:: ".trim($_POST['user_email']);
				$headers = "From: ".trim($_POST['user_name'])." <".trim($_POST['user_email']).">\r\nReply-To:".trim($_POST['user_email']);
				$maildata =wp_mail($to, $subject, $massage, $headers); 
				echo "<script>jQuery('#myformdata').hide();</script>";
				echo "<script>jQuery('#mailsentbox').show();</script>";							
			}
		}
		
		?>
		
		<div class="col-md-3 contact_detail">
			<div class="row">
				<?php the_post(); ?>
				<div class="col-md-12">
					<p><?php the_content( __( 'Read More' , 'copbiz' ) ); ?></p>
					<address>
						<?php if($current_options['contact_address']!='') { echo $current_options['contact_address']; } ?>
						<br> <?php if($current_options['contact_address_two']!='') { echo $current_options['contact_address_two']; } ?>
						<br><?php if($current_options['contact_email']!='') { ?>
						<a href="mailto:<?php  echo $current_options['contact_email']; ?>"> <?php echo $current_options['contact_email']; ?></a>
						<?php } ?>
						<?php if($current_options['contact_phone_number']!='') { ?>
						<br>+<?php echo $current_options['contact_phone_number']; ?>
						<?php } ?>
						<br>
					</address>
				</div>
			</div>
		</div>
    </div>
</div>
<?php 
$mapsrc= $current_options['contact_google_map_url'];	
$mapsrc=$mapsrc.'&amp;output=embed';
if($current_options['contact_google_map_enabled']=="on") {
 ?>
<div class="container">
	<div class="row">
	<div class="col-md-12">
		<div class="google_map">			
			<iframe width="100%" height="400"  src="<?php echo $mapsrc; ?>" ></iframe>	
		</div>
		</div>
	</div>
</div>
<?php }
get_template_part('index','call-out-area'); ?>
<?php get_footer(); ?>