<!--Homepage Mobile Responsive Section-->
<div class="content_responsive_section">
	<div class="container">
		<div id="corpo_content_section_scroll">	
			<?php
			$count_posts = wp_count_posts( 'corpbiz_project')->publish;
			$args = array( 'post_type' => 'corpbiz_project','posts_per_page' =>$count_posts); 	
			$project = new WP_Query( $args );
			if( $project->have_posts() )
			{ 	while ( $project->have_posts() ) : $project->the_post(); ?>
			<div class="row pull-left">
				<div class="col-md-6 content_area">
					<h2><?php the_title(); ?>
						<?php if(get_post_meta( get_the_ID(),'project_title', true )){ ?>
						<span><?php echo get_post_meta( get_the_ID(),'project_title', true ); ?></span>
						<?php } ?>
					</h2>
					<?php if(get_post_meta( get_the_ID(),'project_desciption', true )){ ?>
					<p><?php echo get_post_meta( get_the_ID(),'project_desciption', true ); ?></p>
					<?php } ?>
					<?php if(get_post_meta( get_the_ID(),'project_button_text', true )){ ?>
					<div class="btntop">
						<a href="<?php if(get_post_meta( get_the_ID(),'meta_button_link', true )){ echo get_post_meta( get_the_ID(),'meta_button_link', true ); } ?>" class="btn_red" <?php if(get_post_meta( get_the_ID(),'meta_button_target', true )){ echo "target='_blank'";} ?>><?php echo get_post_meta( get_the_ID(),'project_button_text', true );  ?></a>
					</div>
					<?php } ?>
				</div>				
				<div class="col-md-6 content_img_area">
					<div class="module">
						<?php if(has_post_thumbnail()): ?>
						<?php $defalt_arg =array('class' => "img-responsive content_img"); ?>
						<?php the_post_thumbnail('', $defalt_arg); ?>					
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php endwhile;
			} else {?>
			<div class="row pull-left">
				<div class="col-md-6 content_area">
					<h2><?php _e('We build responsive','corpbiz'); ?> <span><?php _e('mobile-friendly website','corpbiz'); ?></span></h2>
					<p><?php _e('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam scelerisque faucibus risus non iaculis. Fusce a augue ante, pellentesque pretium erat. Fusce in turpis in velit tempor pretium. Integer a leo libero.','corpbiz'); ?></p>
					<div class="btntop">
						<a href="#" class="btn_red"><?php _e('Buy it Now','corpbiz'); ?></a>
					</div>
				</div>
				
				<div class="col-md-6 content_img_area">
					<div class="module"><img class="img-responsive content_img" src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/mobile.png"></div>
				</div>
			</div>		

			<div class="row pull-left">
				<div class="col-md-6 content_area">
					<h2><?php _e('We build responsive','corpbiz'); ?> <span><?php _e('mobile-friendly website','corpbiz'); ?></span></h2>
					<p><?php _e('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam scelerisque faucibus risus non iaculis. Fusce a augue ante, pellentesque pretium erat. Fusce in turpis in velit tempor pretium. Integer a leo libero.','corpbiz'); ?></p>
					<div class="btntop">
						<a href="#" class="btn_red"><?php _e('Buy it Now','corpbiz'); ?></a>
					</div>
				</div>
				
				<div class="col-md-6 content_img_area">
					<div class="module"><img class="img-responsive content_img" src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/mobile.png"></div>
				</div>
			</div>
			<?php } ?>			
		</div>
	</div>	
</div>
<div class="container content_pagi">
	<div class="row">
		<div id="pager1" class="pager testi-pager"></div>
	</div>
</div>
<!--/Homepage Mobile Responsive Section-->