<?php $current_options = get_option('corpbiz_options',theme_data_setup());  ?>
<div class="portfolio-column-section">
	<div class="container">
		<?php 
		$post_type = 'corpbiz_portfolio';
		$tax = 'cor_portfolio_categories'; 
		$term_args=array( 'hide_empty' => true);
		$tax_terms = get_terms($tax, $term_args);
		$defualt_tex_id = get_option('custom_texo_corpbiz');
		$j=1;
		$tab='';
		if(isset($_GET['div']))
		{
			$tab=$_GET['div'];
		}
	?>
		<!--Portfolio Tabs-->
		<div class="col-md-12 portfolio_tabs_section">
			<ul class="portfolio_tabs" id="mytabs">
				<span><?php _e('Filter :','corpbiz'); ?></span>
				<?php	foreach ($tax_terms  as $tax_term) { 
				$tax_term_name = str_replace(' ', '_', $tax_term->name);
				$tax_term_name = preg_replace('~[^A-Za-z\d\s-]+~u', 'qua', $tax_term_name);
				?>
				<li <?php if($tax_term->term_id == $defualt_tex_id) echo "class='active'"; ?>><a data-toggle="tab" href="#<?php echo $tax_term_name; ?>"><?php echo $tax_term->name; ?></a></li>
				<?php } ?>				
			</ul>
		</div>
		<!--/Portfolio Tabs-->
		
		<!-- Portfolio Area -->
		<div class="row tab-content corpo_main_section" id="myTabContent">
		<?php 
		
		global $paged;
		$curpage = $paged ? $paged : 1;
		
		$norecord=0;
		$total_posts=0;
		$min_post_start=0;
		$is_active=true;
		if ($tax_terms) 
		{ 	foreach ($tax_terms  as $tax_term)
			{	
				if(isset($_POST['total_posts']))
				{
					$count_posts = $_POST['total_posts'];
				}
				else
				{
					//$count_posts = wp_count_posts( $post_type)->publish; 
					if(is_page_template('portfolio-2-col.php')) {$count_posts = 4;}
					if(is_page_template('portfolio-3-col.php')) {$count_posts = 6;}
					if(is_page_template('portfolio-4-col.php')) {$count_posts = 8;}
					
				}
				if(isset($_POST['min_post_start']))
				{
					$min_post_start = $_POST['min_post_start'];
				}
				
				$total_posts=$count_posts;
				
				 $args = array (
				'max_num_pages' =>5, 
				'post_status' => 'publish',
				'post_type' => $post_type,
				'portfolio_categories' => $tax_term->slug,
				'posts_per_page' =>$count_posts,
				'paged' => $curpage,
				);
				
			global $j;
			$j=1;
			$portfolio_query = null;		
			$portfolio_query = new WP_Query($args);	
				
			if( $portfolio_query->have_posts() )
			{  ?>
			<div id="<?php echo $tax_term->slug; ?>" class="tab-pane fade in <?php if($tab==''){if($is_active==true){echo 'active';}$is_active=false;}else if($tab==$tax_term->slug){echo 'active';} ?>">
				<div class="row portfolio_row">
					<?php
					while ($portfolio_query->have_posts()) { $portfolio_query->the_post();
					if(get_post_meta( get_the_ID(),'project_more_btn_link', true )) 
					{ $project_more_btn_link = get_post_meta( get_the_ID(),'project_more_btn_link', true );
					} else {
					$project_more_btn_link = '';
					} ?>
					
					
					<?php if(is_page_template('portfolio-2-column.php')) { ?>
					<div class="col-md-6 col-md-6 port_col_area">
					<?php } ?>
					
					<?php if(is_page_template('portfolio-3-column.php')) { ?>
					<div class="col-md-4 col-md-6 port_col_area">
					<?php } ?>
					
					<?php if(is_page_template('portfolio-4-column.php')) { ?>
					<div class="col-md-3 col-md-6 port_col_area">
					<?php } ?>
						
						<div class="portfolio_image">
							<?php if(is_page_template('portfolio-2-column.php')) { 
							corpbiz_image_thumbnail('','img-responsive');
							} ?>
							
							<?php if(is_page_template('portfolio-3-column.php')) {
							corpbiz_image_thumbnail('','img-responsive');
							} ?>
							
							<?php if(is_page_template('portfolio-4-column.php')) { 
							corpbiz_image_thumbnail('','img-responsive');
							} ?>

							<?php
							if(has_post_thumbnail())
							{ 
							$post_thumbnail_id = get_post_thumbnail_id();
							$post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id );
							} ?>
							<div class="portfolio_showcase_overlay">
										<div class="portfolio_showcase_overlay_inner">
											<div class="portfolio_showcase_icons">
												<a class="hover_thumb" title="<?php the_title(); ?>" data-lightbox="image" href="<?php echo $post_thumbnail_url; ?>"><i class="fa fa-plus"></i></a>
											</div>
										</div>
									</div>
							</div>
						<div class="portfolio_caption">
									<a <?php if(get_post_meta( get_the_ID(),'meta_portfolio_target', true )) { echo 'target="_blank"'; } ?> href="<?php echo $meta_portfolio_link; ?>"> <?php echo the_title(); ?></a>
									<?php if(get_post_meta( get_the_ID(),'portfolio_summary', true )) 
									{ 
										echo "<span>".get_post_meta( get_the_ID(),'portfolio_summary', true ) ."</span>";
									} ?>
								</div>
						
					 </div>
					 <?php 
		// call clearfix css class		
	    corpbiz_portfolio_clearfix($j);  
		
			$norecord=1; } ?>
			</div>
			</div>
			<?php } }  
		?>
		<?php wp_reset_query(); ?>
		
			</div>
			<!-- /Load More Projects Btn -->			
			</div>
		</div>
		<!-- /Portfolio Area1 -->
			<?php } ?>			
	</div>
</div>
<!-- /Portfolio Section -->
<?php get_footer(); ?>