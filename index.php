<?php 
get_header();
get_template_part('index', 'banner'); ?>
<!-- Blog & Sidebar Section -->
<div class="container">
	<div class="row blog_sidebar_section">
		<!--Blog-->
		<div class="<?php corpbiz_post_layout_class(); ?>" >
			<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array( 'post_type' => 'post','paged'=>$paged);		
				$post_type_data = new WP_Query( $args );
					while($post_type_data->have_posts()){
					$post_type_data->the_post();
					get_template_part('content','');
					}  				
					$Webriti_pagination = new Webriti_pagination();
					$Webriti_pagination->Webriti_page($paged, $post_type_data);					
				?>
		</div>
		<?php get_sidebar(); ?>
		</div>
	</div>	
<?php get_footer(); ?>