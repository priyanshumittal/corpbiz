<?php
/**
* @Theme Name	:	Corpbiz-Pro
* @file         :	theme_stup_data.php
* @package      :	Corpbiz-Pro
* @author       :	Hari Maliya
* @license      :	license.txt
* @filesource   :	wp-content/themes/corpbiz/theme_stup_data.php
*/
function theme_data_setup()
{	
	return $theme_options=array(
			//Logo and Fevicon header			
			'front_page'  => 'on',			
			'upload_image_logo'=>'',
			'height'=>'50',
			'width'=>'100',
			'text_title'=>'on',
			'upload_image_favicon'=>'',
			'google_analytics'=>'',
			'webrit_custom_css'=>'',
			
			//Slider
			'home_slider_enabled'=>'on',
			'animation' => 'fade',
			'animationSpeed' => '1500',
			'slideshowSpeed' => '2500',
			'slider_list'=>'',
			'total_slide'=>'',
			
			
			
			'home_banner_enabled'=>'on',
			'home_post_enabled' => 'on',
			'slider_total' => 4,
			'slider_radio' => 'demo',
			'slider_options'=> 'fade',
			'slider_transition_delay'=> '2000',
			'slider_select_category' => 'Uncategorized',
			'featured_slider_post' => '',
			

			// front page 
			'front_page_data'=>array('site-info','service','project-slider','portfolio','testimonial','help-support','call-out-area'),
			
			//Slide
			'home_slider_bg_image'=>'',
			'home_slider_enabled'=>'on',
			'animation' => 'slide',								
			'animationSpeed' => '1500',
			'slide_direction' => 'horizontal',
			'slideshowSpeed' => '2500',
		
			//Site info
			'site_title_one'=>'40+',
			'site_title_two'=>'Sample Pages',
			'site_description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam scelerisque faucibus risus non iaculis. Fusce a augue ante, pellentesque pretium erat. Fusce in turpis in velit tempor pretium. Integer a leo libero',
			'siteinfo_button_one_text'=>'Buy it now',
			'siteinfo_button_one_link'=>'#',
			'siteinfo_button_two_text'=>'View Portfolio',
			'siteinfo_button_two_link'=>'#',
			'siteinfo_button_one_target'=>'on',
			'siteinfo_button_two_target'=>'on',
			
			
			// portfolio 
			'portfolio_title' =>'Our Work Speaks Thousand Words',
			'portfolio_description' =>'We have successfully completed over 2500 projects in mobile and web. Here are few of them.',
			
			// Service
			'service_list' => 4,
			'home_service_title'=>'Our Nice Services',
			'home_service_description' =>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam scelerisque faucibus risus non iaculis.',
			
			// Theme help and support section
			'home_theme_support_title'=>'We are here to help you',
			'home_theme_support_description'=>'24+7 hours support by us',
			'home_theme_support_bg'=>'',
			
			'home_support_icon_one'=>'fa-meh-o',
			'home_support_title_one'=>'Need Support?',
			'home_support_desciption_one'=>'Lorem ipsum dolor sit amet, consectetur faucibus risus non iaculis. Fusce a augue Fusce in turpis in',
			'home_support_learn_more_text_one'=>'learn more',
			'home_support_learn_more_link_one'=>'#',
			'home_support_learn_more_target_one'=>'on',
			
			'home_support_icon_two'=>'fa-list-ol',
			'home_support_title_two'=>'Check Our Forum',
			'home_support_desciption_two'=>'Lorem ipsum dolor sit amet, consectetur faucibus risus non iaculis. Fusce a augue Fusce in turpis in',
			'home_support_learn_more_text_two'=>'learn more',
			'home_support_learn_more_link_two'=>'#',
			'home_support_learn_more_target_two'=>'on',
			
			'home_support_icon_three'=>'fa-support',
			'home_support_title_three'=>'Get Updated',
			'home_support_desciption_three'=>'Lorem ipsum dolor sit amet, consectetur faucibus risus non iaculis. Fusce a augue Fusce in turpis in',
			'home_support_learn_more_text_three'=>'learn more',
			'home_support_learn_more_link_three'=>'#',
			'home_support_learn_more_target_three'=>'on',
			
			//	testimonial			
			'home_testimonial_title'=>'What Our Clients Say',
			'home_testimonial_desciption'=>'lets see what we hear from our valuable clients',
			
			//Footer call out area
			'call_out_title'=>'Get your app ideas transformed into reality',
			'call_out_text'=>'Lorem ipsum dolor sit amet, consectetur faucibus risus non iaculis. Fusce a augue Fusce in turpis in.',
			'call_out_button_text'=>'Buy it Now',
			'call_out_button_link'=>'#',
			'call_out_button_link_target' =>'on',

			//client
			'client_list'=>'4',
			'total_client'=>'',
			'home_client_title'=>'What Our Clients Say',
			'home_client_desciption'=>'lets see what we hear from our valuable clients',
			
			
			//contact page settings				
			'send_usmessage' => 'Contact Us',
			'our_office_enabled'=>'on',
			'contact_address'=>'138, AtlantisLnKingsport ',
			'contact_address_two'=>'Illinois. 121164 ',
			'contact_phone_number'=>'1 800 559 6580 ',			
			'contact_email'=>'themes@webriti.com',
				
			'contact_google_map_enabled'=>'on',
			'contact_google_map_url' => 'https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Kota+Industrial+Area,+Kota,+Rajasthan&amp;aq=2&amp;oq=kota+&amp;sll=25.003049,76.117499&amp;sspn=0.020225,0.042014&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear=Kota+Industrial+Area,+Kota,+Rajasthan&amp;z=13&amp;ll=25.142832,75.879538',
			
			
			
			'enable_custom_typography'=>'off',				
			
			// general typography			
			'general_typography_fontsize'=>'16',
			'general_typography_fontfamily'=>'RobotoLight',
			'general_typography_fontstyle'=>"",
			
			// menu title
			'menu_title_fontsize'=>'15',
			'menu_title_fontfamily'=>'RobotoMedium',
			'menu_title_fontstyle'=>"",
			
			// post title
			'post_title_fontsize'=>'32',
			'post_title_fontfamily'=>'RobotoLight',
			'post_title_fontstyle'=> "",
					
			// Service  title
			'service_title_fontsize'=>'20',
			'service_title_fontfamily'=>'RobotoMedium',
			'service_title_fontstyle'=>"",
			
			// Potfolio  title Widget Heading Title
			'portfolio_title_fontsize'=>'20',
			'portfolio_title_fontfamily'=>'RobotoMedium',
			'portfolio_title_fontstyle'=>"",
			
			// Widget Heading Title
			'widget_title_fontsize'=>'28',
			'widget_title_fontfamily'=>'RobotoLight',
			'widget_title_fontstyle'=>"",
			
			// Call out area Title   
			'calloutarea_title_fontsize'=>'36',
			'calloutarea_title_fontfamily'=>'RobotoLight',
			'calloutarea_title_fontstyle'=>"",
			
			// Call out area descritpion      
			'calloutarea_description_fontsize'=>'15',
			'calloutarea_description_fontfamily'=>'RobotoRegular',
			'calloutarea_description_fontstyle'=>"",
			
			'footer_copyright_text'=> '<p>All Rights Reserved by Corpbiz. Designed and Developed by <a href="http://www.webriti.com/" target="_blank">Wordpress Theme</a>.</p>',
			
			//Taxonomy Archive Portfolio
			'taxonomy_portfolio_list' => 2,
		);
}
?>