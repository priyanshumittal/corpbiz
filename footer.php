<!--Footer Section-->
<?php $current_options = get_option('corpbiz_options',theme_data_setup());?>
<div class="footer_section">
	<div class="container">
		<div class="row">
			<?php 
			if ( is_active_sidebar( 'footer-widget-area' ) )
			{ dynamic_sidebar( 'footer-widget-area' );	}
			else 
			{ ?>
			<div class="col-md-4 col-sm-6 footer_widget_column">		
				<h3 class="footer_widget_title">
					<img class="img-responsive" src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/footer-logo.png" alt="footer logo" />
				</h3>				
				<p><?php _e('Lorem ipsum dolor sit amet, consecteturs non faucibus risus non iaculis. Fusce a augueon Fusce in turpis in','corpbiz'); ?></p>
				<p><?php _e('Lorem ipsum dolor sit amet, consectetur sectetur faucibus risus non iaculis.','corpbiz'); ?></p>
				<address>
				 <p><i class="fa fa-home"></i> <?php _e('E104 Dharti II, Newyourk , United States','corpbiz'); ?></p>
				 <p><i class="fa fa-mobile"></i> +1 (555) 123456789</p>
				</address>
			</div>			
			<div class="col-md-4 col-sm-6 footer_widget_column">		
				<h3 class="footer_widget_title"><?php _e('Quick Links','corpbiz'); ?></h3>				
				<div class="footer_widget_link_tab">
					<a href="#"><i class="fa fa-circle-o"></i><?php _e('This is Health Center Responsive.','corpbiz'); ?></a>
					<a href="#"><i class="fa fa-circle-o"></i><?php _e('This is Health Center Responsive.','corpbiz'); ?></a>
					<a href="#"><i class="fa fa-circle-o"></i><?php _e('This is Health Center Responsive.','corpbiz'); ?></a>
					<a href="#"><i class="fa fa-circle-o"></i><?php _e('This is Health Center Responsive.','corpbiz'); ?></a>
					<a href="#"><i class="fa fa-circle-o"></i><?php _e('This is Health Center Responsive.','corpbiz'); ?></a>
					<a href="#"><i class="fa fa-circle-o"></i><?php _e('This is Health Center Responsive.','corpbiz'); ?></a>
				</div>
			</div>			
			<div class="col-md-4 col-sm-6 footer_widget_column">		
				<h3 class="footer_widget_title"><?php _e('Latest Tweets','corpbiz'); ?></h3>				
				<div class="footer_tweeter_link">
					<div class="pull-left tweet_post"><i class="fa fa-twitter"></i></div>
					<div class="media-body">
						<?php _e('Lorem ipsum','corpbiz'); ?> <a href="#"><?php _e('@dolor','corpbiz'); ?></a><?php _e('sit amet, consecteturs non faucibus.','corpbiz'); ?>
						<div class="tweet_hour">
							<a href="#" class="tweet_date"><?php _e('2 hours ago','corpbiz'); ?></a>
						</div>
					</div>
				</div>
				<div class="footer_tweeter_link">
					<div class="pull-left tweet_post"><i class="fa fa-twitter"></i></div>
					<div class="media-body">
						<?php _e('Lorem ipsum','corpbiz'); ?> <a href="#"><?php _e('@dolor','corpbiz'); ?></a><?php _e('sit amet, consecteturs non faucibus.','corpbiz'); ?>
						<div class="tweet_hour">
							<a href="#" class="tweet_date"><?php _e('2 hours ago','corpbiz'); ?></a>
						</div>
					</div>
				</div>
				<div class="footer_tweeter_link">
					<div class="pull-left tweet_post"><i class="fa fa-twitter"></i></div>
					<div class="media-body">
						<?php _e('Lorem ipsum','corpbiz'); ?> <a href="#"><?php _e('@dolor','corpbiz'); ?></a><?php _e('sit amet, consecteturs non faucibus.','corpbiz'); ?>
						<div class="tweet_hour">
							<a href="#" class="tweet_date"><?php _e('2 hours ago','corpbiz'); ?></a>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
<!--/Footer Section-->


<!--Footer Copyright Section-->
<div class="container">
	<div class="row copyright_menu_section">
		<?php if($current_options['footer_copyright_text']!='') { ?>
		<div class="col-md-6">			
			<p> <?php echo $current_options['footer_copyright_text']; ?></p>
		</div>	
		<?php } ?>
		<div class="col-md-6">			
		<?php	wp_nav_menu( array(  
						'theme_location' => 'secondary',
						'menu_class' => 'footer_menu_links',
						'fallback_cb' => 'webriti_fallback_page_menu',
						'walker' => new webriti_nav_walker()
					));	
			?>
		</div>
	</div>
</div>
<?php
if($current_options['webrit_custom_css']!='') {  ?>
<style>
<?php echo $current_options['webrit_custom_css']; ?>
</style>
<?php } 
if($current_options['google_analytics']!='') {  ?>
<script type="text/javascript">
<?php echo $current_options['google_analytics']; ?>
</script>
<?php } ?>	
<?php wp_footer(); ?>
  </body>
</html>