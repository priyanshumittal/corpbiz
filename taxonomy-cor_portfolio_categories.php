<?php
get_header();
get_template_part('index', 'banner');
$current_options = get_option('corpbiz_options',theme_data_setup());
 ?>
 <div class="container">
	<div class="row">
	
	</div>	
	
	<div class="row tab-content corpo_main_section" id="myTabContent">
		<div class="row portfolio_row">
			<?php $i=1;
				while( have_posts() ) : the_post();
					 if($current_options['taxonomy_portfolio_list']==2) {
							if(get_post_meta( get_the_ID(),'meta_portfolio_link', true )) 
							{ $meta_portfolio_link=get_post_meta( get_the_ID(),'meta_portfolio_link', true ); }
							else 
							{ $meta_portfolio_link = get_post_permalink(); } ?>
							<div class="col-md-6 col-sm-6 port_col_area">
								<div class="portfolio_image"><?php 
									if(has_post_thumbnail())
									{ 	$class=array('class'=>'img-responsive');
										the_post_thumbnail('', $class);
										$post_thumbnail_id = get_post_thumbnail_id();
										$post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id );
									?>
									<div class="portfolio_showcase_overlay">
										<div class="portfolio_showcase_overlay_inner">
											<div class="portfolio_showcase_icons">
												<a class="hover_thumb" title="<?php the_title(); ?>" data-lightbox="image" href="<?php echo $post_thumbnail_url; ?>"><i class="fa fa-plus"></i></a>
											</div>
										</div>
									</div>
									<?php } ?>
								</div>
								<div class="portfolio_caption">
									<a <?php if(get_post_meta( get_the_ID(),'meta_portfolio_target', true )) { echo 'target="_blank"'; } ?> href="<?php echo $meta_portfolio_link; ?>"> <?php echo the_title(); ?></a>
									<?php if(get_post_meta( get_the_ID(),'portfolio_summary', true )) 
									{ 
										echo "<span>".get_post_meta( get_the_ID(),'portfolio_summary', true ) ."</span>";
									} ?>
								</div>
							</div>
							<?php } ?>
							
							<?php if($current_options['taxonomy_portfolio_list']==3) {
							if(get_post_meta( get_the_ID(),'meta_portfolio_link', true )) 
							{ $meta_portfolio_link=get_post_meta( get_the_ID(),'meta_portfolio_link', true ); }
							else 
							{ $meta_portfolio_link = get_post_permalink(); } ?>
							<div class="col-md-4 col-sm-6 port_col_area">
								<div class="portfolio_image"><?php 
									if(has_post_thumbnail())
									{ 	$class=array('class'=>'img-responsive');
										the_post_thumbnail('', $class);
										$post_thumbnail_id = get_post_thumbnail_id();
										$post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id );
									?>
									<div class="portfolio_showcase_overlay">
										<div class="portfolio_showcase_overlay_inner">
											<div class="portfolio_showcase_icons">
												<a class="hover_thumb" title="<?php the_title(); ?>" data-lightbox="image" href="<?php echo $post_thumbnail_url; ?>"><i class="fa fa-plus"></i></a>
											</div>
										</div>
									</div>
									<?php } ?>
								</div>
								<div class="portfolio_caption">
									<a <?php if(get_post_meta( get_the_ID(),'meta_portfolio_target', true )) { echo 'target="_blank"'; } ?> href="<?php echo $meta_portfolio_link; ?>"> <?php echo the_title(); ?></a>
									<?php if(get_post_meta( get_the_ID(),'portfolio_summary', true )) 
									{ 
										echo "<span>".get_post_meta( get_the_ID(),'portfolio_summary', true ) ."</span>";
									} ?>
								</div>
							</div>
							<?php } ?>
							
							
				<?php if($current_options['taxonomy_portfolio_list']==4) {
							if(get_post_meta( get_the_ID(),'meta_portfolio_link', true )) 
								{ $meta_portfolio_link=get_post_meta( get_the_ID(),'meta_portfolio_link', true ); }
							else 
							{ $meta_portfolio_link = get_post_permalink(); } ?>
							<div class="col-md-3 col-sm-6 port_col_area">
								<div class="portfolio_image"><?php 
									if(has_post_thumbnail())
									{ 	$class=array('class'=>'img-responsive');
										the_post_thumbnail('', $class);
										$post_thumbnail_id = get_post_thumbnail_id();
										$post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id );
									?>
									<div class="portfolio_showcase_overlay">
										<div class="portfolio_showcase_overlay_inner">
											<div class="portfolio_showcase_icons">
												<a class="hover_thumb" title="<?php the_title(); ?>" data-lightbox="image" href="<?php echo $post_thumbnail_url; ?>"><i class="fa fa-plus"></i></a>
											</div>
										</div>
									</div>
									<?php } ?>
								</div>
								<div class="portfolio_caption">
									<a <?php if(get_post_meta( get_the_ID(),'meta_portfolio_target', true )) { echo 'target="_blank"'; } ?> href="<?php echo $meta_portfolio_link; ?>"> <?php echo the_title(); ?></a>
									<?php if(get_post_meta( get_the_ID(),'portfolio_summary', true )) 
									{ 
										echo "<span>".get_post_meta( get_the_ID(),'portfolio_summary', true ) ."</span>";
									} ?>
								</div>
							</div>
							<?php } ?>
	<?php if($i%$current_options['taxonomy_portfolio_list']==0)	{ echo "<div class='clearfix'></div>"; 	}
						$i++; endwhile; ?>
					</div>
				</div>
				<?php 
		 /* end term wise data */	wp_reset_query();
	 // end for-each tax_terms
 // end of text data 
	?>
	</div>
<?php 
get_template_part('index','call-out-area');
get_footer(); ?>