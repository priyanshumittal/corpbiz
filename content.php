		<div id="post-<?php the_ID(); ?>" <?php post_class('blog_section'); ?>>
				<?php if(has_post_thumbnail()): ?>
				<?php $defalt_arg =array('class' => "img-responsive"); ?>
				<div class="blog_post_img">					
					<?php the_post_thumbnail('', $defalt_arg); ?>					
				</div>
				<?php endif; ?>
				<div class="post_title_wrapper">
					<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<div class="post_detail">
						<a href="<?php echo get_month_link(get_post_time('Y'),get_post_time('m')); ?>"><i class="fa fa-calendar"></i> <?php echo get_the_date('M j, Y'); ?> </a>
						<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><i class="fa fa-user"></i> <?php _e('Posted by: &nbsp;','corpbiz'); ?> <?php the_author(); ?></a>
						<a href="<?php comments_link(); ?>"><i class="fa fa-comments"></i> <?php comments_number( 'No Comments', '1 Comment', '% Comments' ); ?> </a>
						<?php $tag_list = get_the_tag_list();
						if(!empty($tag_list)) { ?>
						<div class="post_tags">
							<i class="fa fa-tags"></i><?php the_tags('', ', ', '<br />'); ?>							
						</div>
						<?php } ?>
					</div>
				</div>
				<div class="blog_post_content">
					<?php the_content(); ?>
				</div>	
			</div>