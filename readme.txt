Corpbiz.

A Premium multi colored Business Blog theme that supports Primary menu's , Primary sidebar,Four widgets area at the footer region  etc. 
It has a perfect design that's great for any Business/Firms  Blogs who wants a new look for their site. Three page templates Home ,Blog and Contact Page. 
Theme supports featured slider managed from Theme Option Panel.

Author: Priyanshu Mittal,Hari Maliya,Shahid Mansuri and Vibhor Purandare,harish.
Theme Homepage Url:http://webriti.com/demo/wp/corpbiz/

About:
Quality-Pro a theme for business, consultancy firms etc  by Webriti (Author URI: http://www.webriti.com). 

The CSS, XHTML and design is released under GPL:
http://www.opensource.org/licenses/gpl-license.php

Feel free to use as you please. I would be very pleased if you could keep the Auther-link in the footer. Thanks and enjoy.

Appoinment supports Custom Menu, Widgets and 
the following extra features:

 - Pre-installed menu and content colors
 - Responsive
 - Custom sidebars
 - Support for post thumbnails
 - Similar posts feature
 - 4 widgetized areas in the footer
 - Customise Front Page 
 - Custom footer
 - Translation Ready 
 

# Basic Setup of the Theme.
-----------------------------------
Fresh installation!

1. Upload the Quality-Pro Theme folder to your wp-content/themes folder.
2. Activate the theme from the WP Dashboard.
3. Done!
=== Images ===

All images in Quality-Pro are licensed under the terms of the GNU GPL.

# Top Navigation Menu:
- Default the page-links start from the left! Use the Menus function in Dashboard/Appearance to rearrange the buttons and build your own Custom-menu. DO NOT USE LONG PAGE NAMES, Maximum 14 letters/numbers incl. spaces!
- Read more here: http://codex.wordpress.org/WordPress_Menu_User_Guide

=============Page Templates======================
1. Contact  Page Tempalte:- Create a page as you do in WordPress and select the page template with the name 'Contact'


===========Front Page Added with the theme=================
1 It has header(logo + menus),Home Featured Image, services,recent comments widgets and footer.

======Site Title and Description=============
Site Title and its description in not shown on home page besides this both are used above each page / post along with the search field.
	
Support
-------
Do you enjoy this theme? Send your ideas - issues - on the theme formn . Thank you!

@Version 1.3
1. Add Google font.
2. Add Post , Category and Image slider.
3. Feature image add aboutus & Conatct us page on header.
4. Theme Data Setup.
5. Custom css sanitization.
6. Remove resize image Setting.
7. Add content.php file.
8. Add Content-portfolio.php file.
9. Add font-awesome Icon URL.
10. Full width layout use afer remove sidebar.
11. Change option panel Name. 

@Version 1.2
1.  Add taxonomy archive portfolio template.
2.  Set none empty field in option pannel setting.
3.  Add max-width property on portfolio image in style.css.
4.  Updated footer customization field in option pannel.
5.  Removed all the unused resources.

@Version 1.1
1.	Pagination issue fixed in category, archive, author, tag.
2.	Filter post according to category, archive, author, tag.
3.	Add custom icon feature in service section on front page, service template and about us template.
4.	Add number of services feature in service section on front page. 

@Version 1.0
released

# --- EOF --- #