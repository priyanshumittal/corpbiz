<?php  $current_options = get_option('corpbiz_options'); ?>
<!--Footer Callout Section-->
<div class="footer_callout_area">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
			<?php if($current_options['call_out_title'] !='') { ?>
			<h2><?php echo $current_options['call_out_title']; ?></h2>
			<?php } 
			if($current_options['call_out_text'] !='') { ?>
			<p><?php echo $current_options['call_out_text']; ?></p>
			<?php } ?>
		</div>
		<?php if($current_options['call_out_button_text'] !='') { ?>
		<div class="col-md-3">
			<a <?php if($current_options['call_out_button_link_target'] =='on'){ echo "target='_blank'"; } ?> class="btn_red" href="<?php if($current_options['call_out_button_link'] !='') { echo $current_options['call_out_button_link']; } ?>"><?php echo $current_options['call_out_button_text']; ?></a>
		</div>
		<?php } ?>
		</div>
	</div>
</div>