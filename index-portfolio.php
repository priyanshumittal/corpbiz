<?php  $current_options = get_option('corpbiz_options'); ?>
<div class="portfolio_section">
	<div class="container">
		<div class="row">
			<div class="corpo_heading_title">
			<?php if($current_options['portfolio_title'] !='') { ?>
				<h1><?php echo $current_options['portfolio_title'];  ?></h1>
				<?php } ?>
				<?php if($current_options['portfolio_description'] !='') { ?>
				<p><?php echo $current_options['portfolio_description']; ?></p>
				<?php } ?>
			</div>	
		</div> 		
	</div>
	<?php
		$i=1;
		$count_posts = wp_count_posts( 'corpbiz_portfolio')->publish;
		$total_portfolio= $count_posts;
		if($count_posts > 8)
		{	$count_posts = 8;	}
		$args = array( 'post_type' => 'corpbiz_portfolio','posts_per_page' =>$count_posts); 	
		$portfolio = new WP_Query( $args ); 
		if( $portfolio->have_posts() )
		{
		while ( $portfolio->have_posts() ) : $portfolio->the_post();
			if(get_post_meta( get_the_ID(),'meta_portfolio_link', true )) 
			{ $meta_portfolio_link=get_post_meta( get_the_ID(),'meta_portfolio_link', true ); }
			else { $meta_portfolio_link = get_post_permalink(); }	?>	
			<!--Porfolio Showcase-->
			<div class="col-md-3 col-sm-6 corpo_col_padding">
				<div class="corpo_portfolio_image">
									<?php
									if(has_post_thumbnail())
									{ 
									$class=array('class'=>'img-responsive');
									the_post_thumbnail('', $class);
									$post_thumbnail_id = get_post_thumbnail_id();
									$post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id );
									?>
					<div class="corpo_home_portfolio_showcase_overlay">
						<div class="corpo_home_portfolio_showcase_overlay_inner">
							<div class="corpo_home_portfolio_showcase_icons">
								<h4><?php the_title(); ?></h4>
								<a href="<?php echo $meta_portfolio_link; ?>" <?php if(get_post_meta( get_the_ID(),'meta_portfolio_target', true )) { echo 'target="_blank"'; } ?>><i class="fa fa-link"></i></a>
								<a href="<?php echo $post_thumbnail_url; ?>" data-lightbox="image" title="<?php the_title(); ?>" class="hover_thumb"><i class="fa fa-plus"></i></a>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
	<?php if($i%4==0)
			{	echo "<div class='clearfix'></div>"; 	}
			$i++;
		endwhile;	
		} else { 
		for ($i=1; $i<=8; $i++ ) { ?>
		<div class="col-md-3 col-sm-6 corpo_col_padding">
			<div class="corpo_portfolio_image">
				<img class="img-responsive" src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/portfolio/home-port<?php echo $i; ?>.jpg" alt="portfolio"  />
				<div class="corpo_home_portfolio_showcase_overlay">
					<div class="corpo_home_portfolio_showcase_overlay_inner">
						<div class="corpo_home_portfolio_showcase_icons">
							<h4><?php _e('Project Title','corpbiz'); ?></h4>
							<a href="#"><i class="fa fa-link"></i></a>
							<a href="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/portfolio/home-port<?php echo $i; ?>.jpg" data-lightbox="image" title="corpo" class="hover_thumb"><i class="fa fa-plus"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } 
	}
	?>	
	<!--/Porfolio Showcase-->	
</div>
<!--/Homepage Portfolio Section-->