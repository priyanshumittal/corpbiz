<?php 
//Template Name: ABOUT US
get_header();
$current_options = get_option('corpbiz_options');
	 ?>
<!-- Page Section -->
<div class="page_mycarousel" <?php if(get_post_meta( get_the_ID(), 'page_header_image', true )) { ?> style="background: url('<?php echo get_post_meta( get_the_ID(), 'page_header_image', true ); ?>')  repeat scroll 0 0 / cover;" <?php } ?> >
	<div class="container page_title_col">
		<div class="row">
			<div class="hc_page_header_area">
				<h1><?php the_title(); ?></h1>		
			</div>
		</div>
	</div>
</div>
<!-- /Page Section -->
<!--About Buy Now Section-->
<div class="aboutus_buynow_section">
	<div class="container">
		<?php the_post(); 		
		if(has_post_thumbnail()){
		$defalt_arg =array('class' => "img-responsive"); ?>
		<?php the_post_thumbnail('', $defalt_arg); ?>
				 <div class="about-section-space">
				</div>
				<?php } the_content(); ?>							
	</div>	
</div>
<!--/About Buy Now Section-->
<!--Homepage Service Section-->
<?php $current_options = get_option('corpbiz_pro_options'); ?>
<div class="container">
	<div class="row">
		<div class="service_heading_title">
			<?php if($current_options['home_service_title'] !="") { ?>
			<h1><?php echo $current_options['home_service_title']; ?></h1>
			<?php } ?>
			<?php if($current_options['home_service_description'] !="") { ?>
			<p> <?php echo $current_options['home_service_description']; ?> </p>
			<?php } ?>
		</div>	
	</div>
	<div class="row">
		<?php
			$i=1;
			$count_posts = wp_count_posts( 'corpbiz_service')->publish;			
			$args = array( 'post_type' => 'corpbiz_service','posts_per_page' =>$count_posts); 	
			$service = new WP_Query( $args );
			if( $service->have_posts() )
			{ while ( $service->have_posts() ) : $service->the_post(); ?>
			<div class="col-md-3 col-sm-6 homepage_service_section">			
				<?php if(get_post_meta( get_the_ID(),'meta_service_link', true )) 
					{ $meta_service_link=get_post_meta( get_the_ID(),'meta_service_link', true ); }
					else
					{ $meta_service_link = ""; }						
					?>
					<?php if(has_post_thumbnail()){  ?>	
						<div class="service_box">
							<?php if($meta_service_link){
									$defalt_arg =array('class' => "img-responsive"); ?> 
									<a href="<?php echo $meta_service_link;  ?>" <?php if(get_post_meta( get_the_ID(),'meta_service_target', true )) { echo 'target="_blank"'; } ?>> <?php the_post_thumbnail('', $defalt_arg); ?> </a>
							<?php } else {
									$defalt_arg =array('class' => "img-responsive"); 
									the_post_thumbnail('', $defalt_arg);
							} ?>
						</div>
					<?php } else {
						if(get_post_meta( get_the_ID(),'service_icon_image', true )) {?>
						<div class="service_box">
						<?php if($meta_service_link){ ?>
						<a href="<?php echo $meta_service_link;  ?>" <?php if(get_post_meta( get_the_ID(),'meta_service_target', true )) { echo 'target="_blank"'; } ?>> <i class="fa <?php echo get_post_meta( get_the_ID(),'service_icon_image', true ); ?>"></i> </a>
						<?php } else { ?>
						<i class="fa <?php echo get_post_meta( get_the_ID(),'service_icon_image', true ); ?>"></i>
						<?php } ?>
						</div>
					<?php }
					} ?>
					<?php if($meta_service_link){ ?>
						<h2><a href="<?php echo $meta_service_link;  ?>" <?php if(get_post_meta( get_the_ID(),'meta_service_target', true )) { echo 'target="_blank"'; } ?>><?php the_title(); ?></a></h2>
					<?php } else { ?>
						<h2><?php the_title(); ?></h2>
					<?php } ?>
				<p><?php echo get_post_meta( get_the_ID(), 'service_description_text', true ); ?></p>
			</div>
		<?php if($i%4==0)
			{	echo "<div class='clearfix'></div>"; 	}
			$i++; endwhile;
		} else { ?>
		<div class="col-md-3 col-sm-6 homepage_service_section">
			<div class="service_box">
				<i class="fa fa-mobile color_green"></i>
			</div>
			<h2><?php _e('Responsive Design','corpbiz'); ?></h2>
			<p><?php _e('Lorem ipsum dolor sit amet, consect adipiscing elit.ivamus eget ...','corpbiz'); ?></p>
		</div>		
		<div class="col-md-3 col-sm-6 homepage_service_section">
			<div class="service_box">
				<i class="fa fa-rocket color_red"></i>
			</div>
			<h2><?php _e('Power full Admin','corpbiz'); ?></h2>
			<p><?php _e('Lorem ipsum dolor sit amet, consect adipiscing elit.ivamus eget ...','corpbiz'); ?></p>
		</div>		
		<div class="col-md-3 col-sm-6 homepage_service_section">
			<div class="service_box">
				<i class="fa fa-thumbs-o-up color_blue"></i>
			</div>
			<h2><?php _e('Great Support','corpbiz'); ?></h2>
			<p><?php _e('Lorem ipsum dolor sit amet, consect adipiscing elit.ivamus eget ...','corpbiz'); ?></p>
		</div>		
		<div class="col-md-3 col-sm-6 homepage_service_section">
			<div class="service_box">
				<i class="fa fa-laptop color_orange"></i>
			</div>
			<h2><?php _e('Clean Minimal Design','corpbiz'); ?></h2>
			<p><?php _e('Lorem ipsum dolor sit amet, consect adipiscing elit.ivamus eget ...','corpbiz'); ?></p>
		</div>
		<?php } ?>		
	</div>	
</div>
<!--/Homepage Service Section-->

<!--About Team Section-->
<div class="about_team_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12"><h1 class="about_team_title"><?php _e('Our Staff','corpbiz'); ?></h1></div>
		</div>
		<div class="row">
		<?php
			$j=1;
			$count_posts = wp_count_posts( 'corpbiz_team')->publish;
			$arg = array( 'post_type' => 'corpbiz_team','posts_per_page' =>$count_posts);
			$team = new WP_Query( $arg ); 
			if($team->have_posts())
			{	while ( $team->have_posts() ) : $team->the_post();	?>		
			<div class="col-md-3 col-sm-6">
				<div class="about_team_showcase">
				<?php $defalt_arg =array('class' => "img-responsive");
				if(has_post_thumbnail()): ?>
					<?php the_post_thumbnail('', $defalt_arg); ?>
				<?php endif; ?>					
					<div class="caption">
						<h3><?php the_title(); ?></h3>
						<?php if(get_post_meta( get_the_ID(), 'description_meta_save', true ) != '' ) { ?>
						<p><?php echo get_post_meta( get_the_ID(), 'description_meta_save', true ) ; ?></p>
						<?php }
						$fb_meta_save = get_post_meta( get_the_ID(), 'fb_meta_save', true );
						$fb_meta_save_chkbx = get_post_meta( get_the_ID(), 'fb_meta_save_chkbx', true );
						$twt_meta_save = get_post_meta( get_the_ID(), 'twt_meta_save', true );
						$twt_meta_save_chkbx = get_post_meta( get_the_ID(), 'twt_meta_save_chkbx', true );
						$flickr_meta_save = get_post_meta( get_the_ID(), 'flickr_meta_save', true );
						$flickr_meta_save_chkbx =  get_post_meta( get_the_ID(), 'flickr_meta_save_chkbx', true );
						$google_meta_save = get_post_meta( get_the_ID(), 'google_meta_save', true );
						$google_meta_save_chkbx =get_post_meta( get_the_ID(), 'google_meta_save_chkbx', true );	
						?>
						<ul class="about_team_social">
						<?php if($fb_meta_save): 
						if($fb_meta_save_chkbx)
						{	$target ="_blank";  } else { $target ="_self";  } ?>	
						<li class="facebook"><a target="<?php echo $target; ?>" href="<?php if($fb_meta_save){ echo esc_html($fb_meta_save); } ?>"><i class="fa fa-facebook-square"></i></a></li>
						<?php endif; ?>
						<?php if($twt_meta_save): 
						if($twt_meta_save_chkbx)
						{	$target ="_blank";  } else { $target ="_self";  } ?>	
							<li class="twitter"><a target="<?php echo $target; ?>" href="<?php if($twt_meta_save){ echo esc_html($twt_meta_save); } ?>"><i class="fa fa-twitter-square"></i></a></li>
						<?php endif; ?>
						<?php if($google_meta_save): 
						if($google_meta_save_chkbx)
						{	$target ="_blank";  } else { $target ="_self";  } ?>	
						<li class="googleplus"><a target="<?php echo $target; ?>" href="<?php if($google_meta_save){ echo esc_html($google_meta_save); } ?>"><i class="fa fa-google-plus-square"></i></a></li>
						<?php endif; ?>
						<?php if($flickr_meta_save): 
						if($flickr_meta_save_chkbx)
						{	$target ="_blank";  } else { $target ="_self";  } ?>	
						<li class="flickr"><a target="<?php echo $target; ?>" href="<?php if($flickr_meta_save){ echo esc_html($flickr_meta_save); } ?>"><i class="fa fa-flickr"></i></a></li>
						<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
			<?php if($j%4==0)
			{	echo "<div class='clearfix'></div>"; 	}
			$j++; endwhile ;
			}
			else
			{
			for($dp=1; $dp<=4; $dp++) { ?>
			<div class="col-md-3 col-sm-6">
				<div class="about_team_showcase">
				<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/team<?php echo $dp ;?>.jpg" alt="Corpo" class="img-responsive">
					<div class="caption">
						<h3><?php _e('Bradley Grosh','corpbiz'); ?></h3>
						<p><?php _e('Malesuada a viverra ac, pellentesque vitae nunc. Aenean ac leo eget nunc fringilla.','corpbiz'); ?></p>
						<ul class="about_team_social">
						   <li class="facebook"><a href="#"><i class="fa fa-facebook-square"></i></a></li>
						   <li class="twitter"><a href="#"><i class="fa fa-twitter-square"></i></a></li>
						   <li class="googleplus"><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
						   <li class="flickr"><a href="#"><i class="fa fa-flickr"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
			<?php }
			} ?>
		</div>
	</div>
</div>
<!--/About Team Section-->
<?php 
get_template_part('index','testimonial');
get_template_part('index', 'call-out-area');
get_footer();
?>